﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for SqlKeepAndSend
/// </summary>
namespace TFL.DAL
{
    public class SqlKeepAndSend
    {
        public static List<KeepAndSendItem> GetKeepAndSendItems(string ids)
        {
            string countryid = LanguageHelper.GetCountry("-");
            List<KeepAndSendItem> list = new List<KeepAndSendItem>();
            SqlParameter[] parameters = { new SqlParameter("@ids", SqlDbType.VarChar) { Value = ids }, new SqlParameter("@countryid", SqlDbType.VarChar) { Value = countryid } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "SendItems");
            foreach (DataRow item in cDataTable.Rows)
            {
                list.Add(new KeepAndSendItem()
                {
                    ItemId = int.Parse(item["id"].ToString()),
                    ItemTitle = item["title"].ToString(),
                    ItemType = ResourceHelper.GetString(item["type"].ToString()).ToString(),
                    ItemDescription = Homepage.ArticleShortDescription(item["description"].ToString(), 100) + "...",
                    ItemCover = item["cover"].ToString(),
                    Path = item["path"].ToString()
                });
            }
            return list;
        }
    }
}