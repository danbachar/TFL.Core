﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for SqlPopularityReport
/// </summary>
namespace TFL.DAL
{
    public class SqlPopularityReport
    {
        public static List<DataTable> GetPopularityReportResults(int MethodValue, DateTime StartDate, DateTime EndDate, int CampaignTypeid, string campaignNameStr, string Users)
        {
            List<DataTable> results = new List<DataTable>();
            //SqlParameter[] parameters =
            //{
            //    new SqlParameter("@methodid", SqlDbType.Int) {Value = MethodValue},
            //    new SqlParameter("@startdate", SqlDbType.Date) {Value = StartDate},
            //    new SqlParameter("@enddate", SqlDbType.Date) {Value = EndDate},
            //    new SqlParameter("@campaigntypeid", SqlDbType.Int) {Value = CampaignTypeid},
            //    new SqlParameter("@campaignname", SqlDbType.VarChar) {Value = campaignNameStr},
            //    new SqlParameter("@users", SqlDbType.VarChar) {Value = Users}
            //};
            // DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "PopularityReport");



            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("PopularityReport", sqlConnection);


            sqlCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter[] parameters =
           {
            new SqlParameter("@methodid", SqlDbType.Int) {Value = MethodValue},
            new SqlParameter("@startdate", SqlDbType.Date) {Value = StartDate},
            new SqlParameter("@enddate", SqlDbType.Date) {Value = EndDate},
            new SqlParameter("@campaigntypeid", SqlDbType.Int) {Value = CampaignTypeid},
            new SqlParameter("@campaignname", SqlDbType.VarChar) {Value = campaignNameStr},
            new SqlParameter("@users", SqlDbType.VarChar) {Value = Users},
            new SqlParameter("@culture", SqlDbType.VarChar) {Value = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName}
        };
            sqlCommand.Parameters.AddRange(parameters);
            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader;
                DataTable dt = new DataTable();
                sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                //count Details
                dt.Load(sqlDataReader);
                //if (dt.Rows.Count == 0)
                //    throw new Exception("NoResults");


                results.Add(dt);

                //countries Details
                dt = new DataTable();
                dt.Load(sqlDataReader);
                results.Add(dt);
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return results;
        }
    }
}