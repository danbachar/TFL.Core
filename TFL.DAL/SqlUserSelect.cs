﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Data.SqlClient;
using TFL.BO;
using TFL_BL;
using TFL.BO;

/// <summary>
/// Summary description for SqlUserSelect
/// </summary>
namespace TFL.DAL
{
    public class SqlUserSelect
    {
        public static List<TFL.BO.UserSelect> GetUserSelect(string key, int professionType)
        {
            List<UserSelect> list = new List<UserSelect>();
            SqlParameter[] parameters = { new SqlParameter("@key", SqlDbType.VarChar) { Value = key }, new SqlParameter("@professionType", SqlDbType.Int) { Value = professionType } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "GetUsersByName");
            foreach (DataRow user in cDataTable.Rows)
            {
                list.Add(new UserSelect()
                {
                    Id = user["userid"].ToString(),
                    Name = user["full_name"].ToString(),
                    Label = user["full_name"].ToString()
                });
            }
            return list;
        }

        public static List<TFL.BO.UserSearch> SearchFreeText(string key)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;


            List<UserSearch> list = new List<UserSearch>();
            SqlParameter[] parameters = { new SqlParameter("@key", SqlDbType.VarChar) { Value = key.Trim() }, new SqlParameter("@culture", SqlDbType.VarChar) { Value = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "SearchFreeText");
            foreach (DataRow user in cDataTable.Rows)
            {
                list.Add(new UserSearch()
                {
                    FullName = textInfo.ToTitleCase(user["full_name"].ToString()),
                    UserPath = GetPath(Convert.ToInt32(user["rowtype"].ToString()), 
                                        Convert.ToInt32(user["userid"].ToString()), 
                                        user["firstname"].ToString(), user["lastname"].ToString(), 
                                        Convert.ToInt32(user["professionTypeId"].ToString()), 
                                        textInfo.ToTitleCase(user["full_name"].ToString())),
                    Location = GetLocation(Convert.ToInt32(user["rowtype"].ToString()), 
                                            user["CountryId"].ToString(), 
                                            user["CityId"].ToString()),
                    ImagePath = GetImage(Convert.ToInt32(user["rowtype"].ToString()), 
                                        Convert.ToInt32(user["userid"].ToString()), 
                                        user["CityId"].ToString())
                });
            }
            return list;
        }

        private static string GetPath(int rowtype, int id, string firstname = "", string lastname = "", int professionid = -1, string fullname = "")
        {
            switch (rowtype)
            {
                case 1:
                    return MemberPath.GetMemberPath(id, firstname, lastname, professionid);
                case 2:
                    return "Campaign/" + id + "/" + fullname.Replace(" ", "-");
                case 3:
                    return "Article/" + id + "/" + fullname.Replace(" ", "-");
            }
            return "";
        }

        private static string GetLocation(int rowtype, string text1, string text2)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            switch (rowtype)
            {
                case 1:
                    return (text1 != "" ? textInfo.ToTitleCase(text2.ToLower()) + ", " + textInfo.ToTitleCase(text1.ToLower()) : "");
                case 2:
                    return textInfo.ToTitleCase(ResourceHelper.GetString(text1).ToString().ToLower());
                case 3:
                    return textInfo.ToTitleCase(ResourceHelper.GetString(text1).ToString().ToLower());
            }
            return "";
        }

        private static string GetImage(int rowtype, int userid, string text)
        {
            switch (rowtype)
            {
                case 1:
                    return UrlHelper.GetProfileSmall(userid.ToString());
                case 2:
                    return UrlHelper.CampaignUploads(text);
                case 3:
                    return text;
            }
            return "";
        }
    }
}