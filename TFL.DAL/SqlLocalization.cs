﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Resources;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for Localization
/// </summary>
namespace TFL.DAL
{
    public class SqlLocalization
    {
        public static List<Country> GetCountries()
        {
            return ResourceHelper.GetCountries();
        }

        public static List<Country> GetStates(string CountryID)
        {
            return ResourceHelper.GetStates(CountryID);
        }
        public static List<Country> GetCities(string CountryID, string Stateid)
        {
            return ResourceHelper.GetCities(CountryID, Stateid);
        }

        public static List<CitySearch> SearchCities(string query)
        {
            List<CitySearch> list = new List<CitySearch>();
            SqlParameter[] parameters = { new SqlParameter("@key", SqlDbType.VarChar) { Value = query }, new SqlParameter("@culture", SqlDbType.VarChar) { Value = Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "SearchCities");
            int index = 0;
            foreach (DataRow state in cDataTable.Rows)
            {
                if (state["cityid"].ToString() != "")
                {
                    list.Add(new CitySearch()
                    {
                        label = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(state["cityname"].ToString().ToLower() + ", " + state["countryname"].ToString().ToLower()),
                        value = state["countryid"] + ";" + state["cityid"]
                    });
                    index++;
                    if (index > 100)
                        break;

                }

            }
            return list;
        }
    }
}