﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for SqlContact
/// </summary>
namespace TFL.DAL
{
    public class SqlContact
    {
        public static List<ContactClass> SearchContacts(ContactClass contactClass)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("SearchContacts", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@companyname", SqlDbType.VarChar).Value = contactClass.CompanyName;
            sqlCommand.Parameters.Add("@contactname", SqlDbType.VarChar).Value = contactClass.ContactName;
            sqlCommand.Parameters.Add("@worktypeid", SqlDbType.Int).Value = contactClass.WorkType;
            sqlCommand.Parameters.Add("@countryid", SqlDbType.VarChar).Value = contactClass.CountryID;
            sqlCommand.Parameters.Add("@cityid", SqlDbType.VarChar).Value = contactClass.CityID;
            sqlCommand.Parameters.Add("@address", SqlDbType.VarChar).Value = contactClass.Address;
            sqlCommand.Parameters.Add("@email", SqlDbType.VarChar).Value = contactClass.Email;
            sqlCommand.Parameters.Add("@website", SqlDbType.VarChar).Value = contactClass.Website;
            sqlCommand.Parameters.Add("@phone", SqlDbType.VarChar).Value = contactClass.Phone;

            sqlCommand.Parameters.Add("@culture", SqlDbType.VarChar).Value = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;

            DataTable dt = new DataTable();

            try
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;


                sqlConnection.Open();
                SqlDataReader sqlDataReader;
                sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                dt.Load(sqlDataReader);
                List<ContactClass> list = new List<ContactClass>();
                foreach (DataRow row in dt.Rows)
                {
                    list.Add(new ContactClass()
                    {
                        CompanyID = Convert.ToInt32(row["companyid"].ToString()),
                        CompanyName = textInfo.ToTitleCase(row["companyname"].ToString().ToLower()),
                        ContactName = textInfo.ToTitleCase(row["contactname"].ToString().ToLower()),
                        WorkType = Convert.ToInt32(row["worktypeid"].ToString()),
                        WorkTypeName = ResourceHelper.GetString(row["workname"].ToString()).ToString(),
                        CountryID = (row["CountryId"] != "" ? textInfo.ToTitleCase(row["CountryId"].ToString().ToLower()) : ""),
                        CityID = (row["CityId"] != "" ? textInfo.ToTitleCase(row["CityId"].ToString().ToLower()) : ""),
                        Address = textInfo.ToTitleCase(row["Address"].ToString().ToLower()),
                        Website = row["website"].ToString(),
                        Email = row["email"].ToString(),
                        Phone = row["phone"].ToString()
                    });
                }
                return list;
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return null;
        }

        public static SqlUser.RegisterStatus CreateContact(ContactClass contactClass)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("CreateContact", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@returnValue", SqlDbType.Int, 0).Direction = ParameterDirection.ReturnValue;
            sqlCommand.Parameters.Add("@contactid", SqlDbType.Int).Value = contactClass.CompanyID;
            sqlCommand.Parameters.Add("@companyname", SqlDbType.VarChar).Value = contactClass.CompanyName;
            sqlCommand.Parameters.Add("@contactname", SqlDbType.VarChar).Value = contactClass.ContactName;
            sqlCommand.Parameters.Add("@worktypeid", SqlDbType.Int).Value = contactClass.WorkType;
            sqlCommand.Parameters.Add("@countryid", SqlDbType.NVarChar).Value = contactClass.CountryID;
            sqlCommand.Parameters.Add("@cityid", SqlDbType.VarChar).Value = contactClass.CityID;
            sqlCommand.Parameters.Add("@address", SqlDbType.VarChar).Value = contactClass.Address;
            sqlCommand.Parameters.Add("@email", SqlDbType.VarChar).Value = contactClass.Email;
            sqlCommand.Parameters.Add("@website", SqlDbType.VarChar).Value = contactClass.Website;
            sqlCommand.Parameters.Add("@phone", SqlDbType.VarChar).Value = contactClass.Phone;
            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                SqlUser.RegisterStatus status = (SqlUser.RegisterStatus)sqlCommand.Parameters["@returnValue"].Value;
                return status;
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return SqlUser.RegisterStatus.UnexpectedError;
        }

        public static List<ContactClass> GetContactsByIDs(string contactsid)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("GetContactsByID", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@contacts", SqlDbType.VarChar).Value = contactsid;

            DataTable dt = new DataTable();

            try
            {

                sqlConnection.Open();
                SqlDataReader sqlDataReader;
                sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                dt.Load(sqlDataReader);
                List<ContactClass> list = new List<ContactClass>();
                foreach (DataRow row in dt.Rows)
                {
                    list.Add(new ContactClass()
                    {
                        CompanyID = Convert.ToInt32(row["companyid"].ToString()),
                        Email = row["email"].ToString(),
                    });
                }
                return list;
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return null;
        }

        public static void DeleteContact(int companyid)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("DeleteContact", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@contactid", SqlDbType.Int).Value = companyid;
            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }
    }
}