﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;
using TFL.BO.News;
using TFL.BO.News.FullArticle;

/// <summary>
/// Summary description for SqlArticle
/// </summary>
namespace TFL.DAL
{
    public class SqlArticle
    {
        public static int SaveArticle(FullArticle article)
        {
            string[] descAndCover = ConvertTempFilesToArticleFiles(article.ArticleDescription);
            article.ArticleDescription = descAndCover[0];
            article.Tags = AddingNewTags(article.Tags);

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("CreateArticle", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@articleDefaultID", SqlDbType.Int).Value = article.ArticleID;
            sqlCommand.Parameters.Add("@articleName", SqlDbType.VarChar, 255).Value = article.ArticleName;
            sqlCommand.Parameters.Add("@creator", SqlDbType.VarChar).Value = article.Creator;
            sqlCommand.Parameters.Add("@articleTypeid", SqlDbType.Int).Value = article.ArticleTypeID;
            sqlCommand.Parameters.Add("@articleDescription", SqlDbType.VarChar, -1).Value = article.ArticleDescription;
            sqlCommand.Parameters.Add("@articleProfessions", SqlDbType.VarChar, -1).Value = article.Users + ",";
            sqlCommand.Parameters.Add("@articleTags", SqlDbType.VarChar, -1).Value = (article.Tags != "" ? article.Tags.Replace("t", "") + "," : "");
            sqlCommand.Parameters.Add("@isapproved", SqlDbType.Int).Value = article.IsApproved;
            sqlCommand.Parameters.Add("@userCreate", SqlDbType.Int).Value = UserData.GetUserDataCookie().UserID;
            sqlCommand.Parameters.Add("@articleCover", SqlDbType.VarChar, 255).Value = descAndCover[1];
            sqlCommand.Parameters.Add("@articleID", SqlDbType.Int, 0).Direction = ParameterDirection.Output;

            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                return Convert.ToInt32(sqlCommand.Parameters["@articleID"].Value);
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
                ChangeSessionID();
            }
            return -1;
        }

        private static string AddingNewTags(string tags)
        {
            List<string> tagstring = new List<string>();
            if (!string.IsNullOrEmpty(tags))
            {
                foreach (string tag in tags.Split(','))
                {
                    if (tag.StartsWith("t"))
                        tagstring.Add(tag.Replace("t", ""));
                    else if (tag.StartsWith(":-"))
                        tagstring.Add(TagSelect.AddTag(tag.Substring(2)).ToString());
                    else
                        tagstring.Add(tag);
                }
                return string.Join(",", tagstring) + ",";
            }
            else
            {
                return tags;
            }
        }

        private static string[] ConvertTempFilesToArticleFiles(string description)
        {
            string cover = "";
            string moreGuid = Guid.NewGuid().ToString();
            string temppath = UrlHelper.StaticUrlPath("~/TempFiles");
            string articlepath = UrlHelper.StaticUrlPath("~/ArticleUpload");

            MatchCollection matches = Regex.Matches(description, "<img.*?>");
            bool isfirst = true;
            foreach (Match match in matches)
            {
                string file = match.Value;
                if (file.Contains("TempFiles"))
                {
                    string filename = file.Substring(file.IndexOf("TempFiles") + 10, file.IndexOf("alt") - file.IndexOf("TempFiles") - 12);
                    string fromPath = Path.Combine(temppath, filename);
                    string relativepath = moreGuid + "-" + filename.Substring(0, filename.Length - 4) +
                                          Path.GetExtension(filename);
                    string toPath = Path.Combine(articlepath, relativepath);
                    try
                    {

                        File.Move(fromPath, toPath);
                        if (isfirst)
                        {
                            ImageHelper.SaveThumb(toPath, 170, 170);
                            ImageHelper.SaveBigThumb(toPath, 300, 300);
                            isfirst = false;
                            cover = UrlHelper.ArticleUploads(ImageHelper.GetCoverPath(relativepath));
                        }
                        description = description.Replace(UrlHelper.TempUploads(filename),
                            UrlHelper.ArticleUploads(relativepath));
                    }
                    catch (Exception)
                    {
                    }
                }
                else
                {
                    if (isfirst)
                    {
                        string filename = file.Substring(file.IndexOf("ArticleUpload") + 14, file.IndexOf("alt") - file.IndexOf("ArticleUpload") - 16);
                        string filepath = UrlHelper.StaticUrlPath("ArticleUpload/" + filename);
                        if (!File.Exists(filepath))
                            ImageHelper.SaveThumb(filepath, 170, 170);
                        isfirst = false;
                        cover = UrlHelper.ArticleUploads(ImageHelper.GetCoverPath(filename));
                    }
                }
            }
            return new string[] { description, cover };
        }

        private static void ChangeSessionID()
        {
            SessionIDManager Manager = new SessionIDManager();

            string NewID = Manager.CreateSessionID(HttpContext.Current);
            bool redirected = false;
            bool IsAdded = false;
            Manager.SaveSessionID(HttpContext.Current, NewID, out redirected, out IsAdded);
        }

        public static FullArticle GetArticleById(int articleid)
        {
            FullArticle article = new FullArticle();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("GetArticleByID", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@ArticleID", SqlDbType.Int, 0).Value = articleid;
            sqlCommand.Parameters.Add("@culture", SqlDbType.VarChar).Value = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;

            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader;
                DataTable dt = new DataTable();
                sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                //campaign Details
                dt.Load(sqlDataReader);
                if (dt.Rows.Count == 0)
                    throw new Exception("ArticleNotFound");


                article.ArticleName = dt.Rows[0]["articleName"].ToString();
                article.ArticleTypeID = Convert.ToInt32(dt.Rows[0]["articleTypeId"].ToString());
                article.ArticleDescription = dt.Rows[0]["articleDescription"].ToString().Replace("\"", "'").Replace('\n', ' ');
                article.AuthorName = dt.Rows[0]["fullname"].ToString();
                article.AuthorID = Convert.ToInt32(dt.Rows[0]["userid"].ToString());
                article.ArticleTypeName = dt.Rows[0]["articleTypeName"].ToString();
                article.ClientPath = MemberPath.GetMemberPath(Convert.ToInt32(dt.Rows[0]["createuserid"].ToString()), dt.Rows[0]["createfirstname"].ToString(), dt.Rows[0]["createlastname"].ToString(), Convert.ToInt32(dt.Rows[0]["createtypeid"].ToString()));
                article.AuthorType = Convert.ToInt32(dt.Rows[0]["professionTypeId"].ToString());
                article.ArticleCreated = Convert.ToDateTime(dt.Rows[0]["articlecreated"].ToString());
                article.ArticleCover = dt.Rows[0]["articleCover"].ToString();

                //Users
                dt = new DataTable();
                dt.Load(sqlDataReader);
                List<UserSelect> users = (from DataRow row in dt.Rows
                                          select new UserSelect()
                                          {
                                              label = row["full_name"].ToString(),
                                              id = row["professionUserId"].ToString(),
                                              name = row["professionTypeId"].ToString()
                                          }).ToList();
                article.UsersList = users;


                //Tags
                dt = new DataTable();
                dt.Load(sqlDataReader);
                List<TagSelect> tags = (from DataRow row in dt.Rows
                                        select new TagSelect()
                                        {
                                            id = row["tagid"].ToString(),
                                            name = row["tagname"].ToString()
                                        }).ToList();
                article.TagsList = tags;


            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return article;
        }

        public static List<Article> GetNewsByAuthor(int userid, int rowspage, int pagenumber)
        {
            List<Article> list = new List<Article>();
            SqlParameter[] parameters =
            {
            new SqlParameter("@userid", SqlDbType.Int) {Value = userid},
            new SqlParameter("@RowspPage", SqlDbType.Int) {Value = rowspage},
            new SqlParameter("@PageNumber", SqlDbType.Int) {Value = pagenumber}
        };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "GetNewsByAuthor");
            foreach (DataRow article in cDataTable.Rows)
            {
                list.Add(new Article()
                {
                    ArticleID = (long)article["ArticleID"],
                    ArticleName = article["ArticleName"].ToString(),
                    ArticleCreated = Convert.ToDateTime(article["articlecreated"].ToString()).ToString("dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture),
                    ArticleTypeName = ResourceHelper.GetString(article["articleTypeName"].ToString()).ToString(),
                    ArticleDescription = Homepage.ArticleShortDescription(article["ArticleDescription"].ToString(), 180),
                    ArticleCover = article["articlecover"].ToString(),
                    ClientPath = MemberPath.GetMemberPath(Convert.ToInt32(article["clientuserid"].ToString()), article["clientfirstname"].ToString(), article["clientlastname"].ToString(), Convert.ToInt32(article["professionTypeId"].ToString())),
                    ClientName = article["clientfirstname"].ToString() + " " + article["clientlastname"].ToString(),
                    CountOfComments = Convert.ToInt32(article["comments"].ToString())
                });
            }
            return list;
        }

        public static void ChangeArticleStatus(int articleid, int status)
        {
            if (UserData.GetUserDataCookie().UserType != UserData.UserRole.Admin)
                return;
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("ChangeArticleStatus", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@articleid", SqlDbType.Int).Value = articleid;
            sqlCommand.Parameters.Add("@statusid", SqlDbType.Int, 255).Value = status;

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
                ChangeSessionID();
            }
        }

        public static List<Article> SearchArticles(Article article)
        {
            List<Article> list = new List<Article>();
            SqlParameter[] parameters =
            {
             new SqlParameter("@articleName", SqlDbType.VarChar) { Value = article.ArticleName },
             new SqlParameter("@articleCreated", SqlDbType.Date) { Value = (article.ArticleCreated==""?(DateTime?)null:Convert.ToDateTime(article.ArticleCreated)) },
             new SqlParameter("@articleTypeId", SqlDbType.Int) { Value = article.TypeId },
             new SqlParameter("@articleDescription", SqlDbType.VarChar) { Value = article.ArticleDescription },
        };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "SearchArticles");
            foreach (DataRow row in cDataTable.Rows)
            {
                list.Add(new Article()
                {
                    ArticleID = (long)row["ArticleID"],
                    ArticleName = row["ArticleName"].ToString(),
                    ArticleCreated = Convert.ToDateTime(row["articlecreated"].ToString()).ToString("dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture),
                    ArticleTypeName = ResourceHelper.GetString(row["articleTypeName"].ToString()).ToString(),
                    ArticleDescription = Homepage.ArticleShortDescription(row["ArticleDescription"].ToString(), 100),
                    ArticleCover = row["articlecover"].ToString(),
                    ClientPath = MemberPath.GetMemberPath(Convert.ToInt32(row["authoruserid"].ToString()), row["authorfirstname"].ToString(), row["authorlastname"].ToString(), Convert.ToInt32(row["professionTypeId"].ToString())),
                    ClientName = row["authorfirstname"].ToString() + " " + row["authorlastname"].ToString()
                });
            }
            return list;
        }

        public static void DeleteArticle(int id)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("DeleteArticle", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@articleID", SqlDbType.Int).Value = id;

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }
    }
}