﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SqlAgency
/// </summary>
namespace TFL.DAL
{
    public class SqlAgency
    {
        public static List<AgencySelect> SearchAgencies(string query)
        {
            List<AgencySelect> list = new List<AgencySelect>();
            SqlParameter[] parameters = { new SqlParameter("@query", SqlDbType.VarChar) { Value = query } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "SearchAgencies");
            foreach (DataRow agency in cDataTable.Rows)
            {
                list.Add(new AgencySelect()
                {
                    label = agency["AgencyName"].ToString(),
                    value = agency["AgencyID"].ToString()
                });
            }
            return list;
        }
    }
}