﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SqlLanguage
/// </summary>
namespace TFL.DAL
{
    public class SqlLanguage
    {
        public static Dictionary<string, string> GetGoogleCodes()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, new SqlParameter[0], "GetCultures");
            foreach (DataRow row in cDataTable.Rows)
            {
                list.Add(row["cultureid"].ToString(), row["googlecode"].ToString());
            }
            return list;
        }

        public static void AddCulture(string cultureid, string cultureName, string countryid, string googlecode, bool isrighttoleft, string languageNameInEnglish)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("AddCulture", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@cultureid", SqlDbType.VarChar).Value = cultureid;
            sqlCommand.Parameters.Add("@cultureName", SqlDbType.VarChar).Value = cultureName;
            sqlCommand.Parameters.Add("@countryid", SqlDbType.VarChar).Value = countryid;
            sqlCommand.Parameters.Add("@googlecode", SqlDbType.VarChar).Value = googlecode;
            sqlCommand.Parameters.Add("@isrtl", SqlDbType.Bit).Value = isrighttoleft;

            sqlCommand.Parameters.Add("@languageNameInEnglish", SqlDbType.VarChar).Value = languageNameInEnglish;

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            ResourceHelper.ClearCache();
        }

        public static List<LanguageCulture> GetLanguages()
        {
            SqlParameter[] parameters = { };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "GetCultures");
            List<LanguageCulture> list = new List<LanguageCulture>();
            foreach (DataRow culture in cDataTable.Rows)
            {
                list.Add(new LanguageCulture()
                {
                    CultureID = culture["cultureid"].ToString(),
                    CultureName = ResourceHelper.GetString(culture["cultureName"].ToString()),
                    Countryid = culture["countryid"].ToString()
                });
            }
            return list;
        }

        public static void DeleteSelected(string[] id)
        {
            foreach (string culture in id)
            {
                SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
                SqlCommand sqlCommand = new SqlCommand("DeleteCulture", sqlConnection);

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add("@cultureid", SqlDbType.VarChar).Value = culture;
                try
                {
                    sqlConnection.Open();
                    sqlCommand.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    throw e;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }
    }
}