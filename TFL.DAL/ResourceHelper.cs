﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for ResourceHelper
/// </summary>
namespace TFL.DAL
{
    public class ResourceHelper
    {
        public static Dictionary<string, string> GetDictionaryResource(string defaultculture = null)
        {
            string culture = (defaultculture ?? Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName);

            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            try
            {
                if (HttpContext.Current.Cache[culture] == null ||
                    ((Dictionary<string, string>)HttpContext.Current.Cache[culture]).Count == 0)
                {
                    SqlParameter[] parameters = { new SqlParameter("@culture", SqlDbType.VarChar) { Value = culture } };
                    DataTable cDataTable =
                        DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString,
                            parameters, "ResourcesByCulture");
                    foreach (DataRow keyVal in cDataTable.Rows)
                    {
                        if (!dictionary.ContainsKey(keyVal["resourceKey"].ToString()))
                            dictionary.Add(keyVal["resourceKey"].ToString(), keyVal["resourceValue"].ToString());
                    }
                    HttpContext.Current.Cache[culture] = dictionary;
                }
                else
                {
                    dictionary = (Dictionary<string, string>)HttpContext.Current.Cache[culture];
                }
            }
            catch (Exception)
            {
                ClearCache();
                //return GetDictionaryResource();
            }
            return dictionary;
        }

        public static void ClearCache()
        {
            string culture = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            HttpContext.Current.Cache.Remove(culture);
        }

        public static string GetString(string key)
        {
            Dictionary<string, string> dictionary = GetDictionaryResource();
            if (dictionary.ContainsKey(key))
                return dictionary[key];
            else
            {
                dictionary = GetDictionaryResource("en");
                if (dictionary.ContainsKey(key))
                    return dictionary[key];
            }
            return "";
        }

        public static string GetModelPropertiesString(string key)
        {
            string culture = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            SqlParameter[] parameters = { new SqlParameter("@key", SqlDbType.VarChar) { Value = key }, new SqlParameter("@culture", SqlDbType.VarChar) { Value = culture } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "[ResourcesByModelProperty]");
            return cDataTable.Rows.Count > 0 ? cDataTable.Rows[0][0].ToString() : "";
        }

        public static string GetTagByID(string tagid)
        {
            string culture = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            SqlParameter[] parameters = { new SqlParameter("@tagid", SqlDbType.VarChar) { Value = tagid }, new SqlParameter("@culture", SqlDbType.VarChar) { Value = culture } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "ResourcesByTagID");
            return cDataTable.Rows.Count > 0 ? cDataTable.Rows[0][0].ToString() : "";
        }

        public static List<TagSelect> GetAllTags()
        {
            List<TagSelect> list = new List<TagSelect>();
            string culture = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            SqlParameter[] parameters = { new SqlParameter("@tagid", SqlDbType.VarChar) { Value = null }, new SqlParameter("@culture", SqlDbType.VarChar) { Value = culture } };
            DataTable cDataTable =
                DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString,
                    parameters, "ResourcesByTagID");
            foreach (DataRow tag in cDataTable.Rows)
            {
                list.Add(new TagSelect()
                {
                    id = tag["resourceKey"].ToString(),
                    name = tag["resourceValue"].ToString()
                });
            }
            return list;
        }

        public static string GetBigTextString(string key)
        {
            string culture = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            SqlParameter[] parameters = { new SqlParameter("@key", SqlDbType.VarChar) { Value = key }, new SqlParameter("@culture", SqlDbType.VarChar) { Value = culture } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "ResourcesByBigText");
            return cDataTable.Rows.Count > 0 ? cDataTable.Rows[0][0].ToString() : "";
        }

        public static List<Country> GetCountries()
        {
            string culture = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            SqlParameter[] parameters = { new SqlParameter("@culture", SqlDbType.VarChar) { Value = culture } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "ResourcesByCountries");
            List<Country> list = new List<Country>();
            foreach (DataRow country in cDataTable.Rows)
            {
                list.Add(new Country()
                {
                    CountryCode = country["resourceKey"].ToString(),
                    CountryName =
                        Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(
                            country["resourceValue"].ToString().ToLower())
                });
            }
            list.Insert(0, new Country() { CountryCode = "", CountryName = GetString("sSelect") });
            return list;
        }

        public static List<Country> GetStates(string countryID)
        {
            string culture = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            SqlParameter[] parameters = { new SqlParameter("@countryid", SqlDbType.VarChar) { Value = countryID }, new SqlParameter("@culture", SqlDbType.VarChar) { Value = culture } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "GetStatesByCountry");
            List<Country> list = new List<Country>();
            foreach (DataRow country in cDataTable.Rows)
            {
                list.Add(new Country()
                {
                    CountryCode = country["stateid"].ToString(),
                    CountryName =
                        Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(
                            country["statename"].ToString().ToLower())
                });
            }
            list.Insert(0, new Country() { CountryCode = "", CountryName = GetString("sSelect") });
            return list;
        }

        public static List<Country> GetCities(string countryID, string stateid)
        {
            string culture = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            SqlParameter[] parameters = { new SqlParameter("@countryid", SqlDbType.VarChar) { Value = countryID }, new SqlParameter("@stateid", SqlDbType.VarChar) { Value = stateid }, new SqlParameter("@culture", SqlDbType.VarChar) { Value = culture } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "GetCityByState");
            List<Country> list = new List<Country>();
            foreach (DataRow country in cDataTable.Rows)
            {
                list.Add(new Country()
                {
                    CountryCode = country["cityid"].ToString(),
                    CountryName =
                        Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(
                            country["cityname"].ToString().ToLower())
                });
            }
            list.Insert(0, new Country() { CountryCode = "", CountryName = GetString("sSelect") });
            return list;
        }

        public static DataTable GetFromCultureToCulture(string fromculture, string toculture, string tablename)
        {
            SqlParameter[] parameters = { new SqlParameter("@fromculture", SqlDbType.VarChar) { Value = fromculture }, new SqlParameter("@toculture", SqlDbType.VarChar) { Value = toculture }, new SqlParameter("@tablename", SqlDbType.VarChar) { Value = tablename } };
            return DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "ResourcesGetFromCultureToCulture");
        }

        public static void SaveResourcesCulture(DataTable dictionary, string tablename, string culture)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("ResourcesSave", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 0;
            sqlCommand.Parameters.Add("@Languages", SqlDbType.Structured).Value = dictionary;
            sqlCommand.Parameters.Add("@tablename", SqlDbType.VarChar).Value = tablename;
            sqlCommand.Parameters.Add("@culture", SqlDbType.VarChar).Value = culture;

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public static List<ArticleCategory> GetAllArticlesCategories()
        {
            List<ArticleCategory> list = new List<ArticleCategory>();
            string culture = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            SqlParameter[] parameters = { new SqlParameter("@culture", SqlDbType.VarChar) { Value = culture } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "ResourcesByArticleCategory");
            foreach (DataRow category in cDataTable.Rows)
            {
                list.Add(new ArticleCategory()
                {
                    id = category["articletypeid"].ToString(),
                    name = category["categoryname"].ToString()
                });
            }
            return list;
        }

        public static List<CampaignCategory> GetAllCampaignsCategories()
        {
            List<CampaignCategory> list = new List<CampaignCategory>();
            string culture = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            SqlParameter[] parameters = { new SqlParameter("@culture", SqlDbType.VarChar) { Value = culture } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "ResourcesByCampaignCategory");
            foreach (DataRow category in cDataTable.Rows)
            {
                list.Add(new CampaignCategory()
                {
                    id = category["campaigntypeid"].ToString(),
                    name = category["categoryname"].ToString()
                });
            }
            return list;
        }
    }

    public class KeyVal
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}