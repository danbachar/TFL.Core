﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using System.Web.Security;

/// <summary>
/// Summary description for SqlUser
/// </summary>
namespace TFL.DAL
{
    public class SqlUser
    {
        private static string validationKey = "C50B3C89CB21F4F1422FF158A5B42D0E8DB8CB5CDA1742572A487D9401E3400267682B202B746511891C1BAF47F8D25C07F6C39A104696DB51F17C529AD3CABE";

        private static string decryptionKey = "8A9BE8FD67AF6979E7D20198CFEA50DD3D3799C77AF2B72F";

        private static int CountAttemptsToLogin = 3;

        private static int WindowAttemptsInMinutes = 0;

        public enum LoginStatus
        {
            Success,
            EmailOrPasswordIncorrect,
            Locked,
            NotApproved
        }

        public enum RegisterStatus
        {
            SuccessRegister,
            NonUniqueEmail,
            UnexpectedError,
            CreateSuccessfuly,
            UpdatedSuccessfuly
        }

        public static UserData Login(string email, string password)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("Login", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@returnValue", SqlDbType.Int, 0).Direction = ParameterDirection.ReturnValue;
            sqlCommand.Parameters.Add("@email", SqlDbType.NVarChar, 255).Value = email;
            sqlCommand.Parameters.Add("@password", SqlDbType.NVarChar, 255).Value = EncodePassword(password);
            sqlCommand.Parameters.Add("@CountAttemptsToLogin", SqlDbType.Int).Value = CountAttemptsToLogin;
            sqlCommand.Parameters.Add("@WindowAttemptsInMinutes", SqlDbType.Int).Value = WindowAttemptsInMinutes;

            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(sqlDataReader);
                LoginStatus status = (LoginStatus)sqlCommand.Parameters["@returnValue"].Value;
                if (status == LoginStatus.Success)
                    return new UserData()
                    {
                        FirstName = dt.Rows[0]["firstname"].ToString(),
                        LastName = dt.Rows[0]["lastname"].ToString(),
                        Email = dt.Rows[0]["email"].ToString(),
                        LoginStatus = status,
                        UserType = (UserData.UserRole)Convert.ToInt32(dt.Rows[0]["professionTypeId"].ToString()),
                        CultureName = dt.Rows[0]["CultureName"].ToString(),
                        UserID = Convert.ToInt32(dt.Rows[0]["userid"].ToString()),
                        UserPath = MemberPath.GetMemberPath(Convert.ToInt32(dt.Rows[0]["userid"].ToString()), dt.Rows[0]["firstname"].ToString(), dt.Rows[0]["lastname"].ToString(), Convert.ToInt32(dt.Rows[0]["professionTypeId"].ToString())),
                        CountryID = dt.Rows[0]["countryid"].ToString(),
                        CityID = dt.Rows[0]["cityid"].ToString(),
                    };
                else
                {
                    return new UserData() { LoginStatus = status };
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return null;
        }

        private static string EncodePassword(string password)
        {
            //string encodedPassword = password;
            //HMACSHA1 hash = new HMACSHA1();
            //hash.Key = HexToByte(validationKey);
            //encodedPassword = Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));
            //return encodedPassword;
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(password);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    password = Convert.ToBase64String(ms.ToArray());
                }
            }
            return password;
        }

        private static string DecodePassword(string password)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(password);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    password = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return password;
        }

        private static byte[] HexToByte(string hexString)
        {
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        public static RegisterStatus CreateUser(string first, string last, int professionTypeId, bool gender, string birth, string countryid, string cityid, string biography, string website, string email, string password, bool newsletter, string cultureName, string guid, ModelProperty modelProperty = null, List<Agency> agencies = null, int defaultUserid = -1)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("CreateUser", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@returnValue", SqlDbType.Int, 0).Direction = ParameterDirection.ReturnValue;
            sqlCommand.Parameters.Add("@defaultUserid", SqlDbType.Int).Value = defaultUserid;
            sqlCommand.Parameters.Add("@firstname", SqlDbType.NVarChar, 255).Value = first;
            sqlCommand.Parameters.Add("@lastname", SqlDbType.NVarChar, 255).Value = last;
            sqlCommand.Parameters.Add("@professionTypeId", SqlDbType.Int).Value = professionTypeId;
            sqlCommand.Parameters.Add("@gender", SqlDbType.Bit).Value = gender;
            sqlCommand.Parameters.Add("@birthdate", SqlDbType.DateTime).Value = birth;
            sqlCommand.Parameters.Add("@countryid", SqlDbType.VarChar).Value = countryid;
            sqlCommand.Parameters.Add("@cityid", SqlDbType.VarChar).Value = cityid;
            sqlCommand.Parameters.Add("@biography", SqlDbType.NVarChar, -1).Value = biography;
            sqlCommand.Parameters.Add("@website", SqlDbType.NVarChar, 255).Value = website;
            sqlCommand.Parameters.Add("@email", SqlDbType.NVarChar, 255).Value = email;
            sqlCommand.Parameters.Add("@password", SqlDbType.NVarChar, 255).Value = EncodePassword(password);
            sqlCommand.Parameters.Add("@agreeForMails", SqlDbType.Bit).Value = newsletter;
            sqlCommand.Parameters.Add("@CultureName", SqlDbType.NVarChar, 5).Value = cultureName;
            sqlCommand.Parameters.Add("@Guid", SqlDbType.NVarChar, 255).Value = guid;
            sqlCommand.Parameters.Add("@useridnew", SqlDbType.Int, 0).Direction = ParameterDirection.Output;

            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                RegisterStatus status = (RegisterStatus)sqlCommand.Parameters["@returnValue"].Value;
                if ((status == RegisterStatus.SuccessRegister || status == RegisterStatus.UpdatedSuccessfuly) && modelProperty != null)
                {
                    int userid = defaultUserid != -1 ? defaultUserid : Convert.ToInt32(sqlCommand.Parameters["@useridnew"].Value);
                    AddModelProperty(userid, modelProperty);
                }
                if ((status == RegisterStatus.SuccessRegister || status == RegisterStatus.UpdatedSuccessfuly) && agencies != null)
                {
                    int userid = defaultUserid != -1 ? defaultUserid : Convert.ToInt32(sqlCommand.Parameters["@useridnew"].Value);
                    AddModelAgencies(userid, agencies);
                }
                return status;
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return RegisterStatus.UnexpectedError;
        }

        private static void AddModelProperty(int userid, ModelProperty modelProperty)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("AddModelProperties", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
            sqlCommand.Parameters.Add("@hair", SqlDbType.VarChar).Value = modelProperty.Hair;
            sqlCommand.Parameters.Add("@eyesid", SqlDbType.VarChar).Value = modelProperty.Eyes;
            sqlCommand.Parameters.Add("@bust", SqlDbType.Decimal).Value = modelProperty.Bust;
            sqlCommand.Parameters.Add("@height", SqlDbType.Decimal).Value = modelProperty.Height;
            sqlCommand.Parameters.Add("@waist", SqlDbType.Decimal).Value = modelProperty.Waist;
            sqlCommand.Parameters.Add("@hips", SqlDbType.Decimal).Value = modelProperty.Hipe;
            sqlCommand.Parameters.Add("@shoe", SqlDbType.Decimal).Value = modelProperty.Shoe;
            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public static void AddModelAgencies(int userid, List<Agency> agencies)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("agencyid"));
            dt.Columns.Add(new DataColumn("countryid"));
            dt.Columns.Add(new DataColumn("cityid"));
            foreach (Agency agency in agencies)
            {
                dt.Rows.Add(agency.AgencyId, agency.AgencyLocation.Split(';')[0], agency.AgencyLocation.Split(';')[1]);
            }

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("AddModelAgencies", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
            sqlCommand.Parameters.Add("@agenciesTable", SqlDbType.Structured).Value = dt;
            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public static void RemoveModelAgency(int userid, Agency agency)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("RemoveAgencyFromModel", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@modelid", SqlDbType.Int).Value = userid;
            sqlCommand.Parameters.Add("@agency", SqlDbType.Int).Value = agency.AgencyId;
            sqlCommand.Parameters.Add("@countryid", SqlDbType.VarChar).Value = agency.AgencyLocation.Split(';')[0];
            sqlCommand.Parameters.Add("@cityid", SqlDbType.VarChar).Value = agency.AgencyLocation.Split(';')[1];
            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }


        public static string ApproveUserMail(string guid)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("ApproveUserMail", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@Guid", SqlDbType.NVarChar, 255).Value = guid;

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return ResourceHelper.GetString("sApproveCompleted").ToString() + "<br>" + ResourceHelper.GetString("sYouAreRedirectedToHomepage").ToString();
        }

        public static UserData GetUserByID(int userid)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("GetUserByID", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = userid;
            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(sqlDataReader);
                if (dt.Rows.Count == 0)
                    return null;
                return new UserData()
                {
                    FirstName = dt.Rows[0]["firstname"].ToString(),
                    LastName = dt.Rows[0]["lastname"].ToString(),
                    UserID = Convert.ToInt32(dt.Rows[0]["userid"].ToString()),
                    UserType = (UserData.UserRole)int.Parse(dt.Rows[0]["professionTypeId"].ToString())
                };
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return null;
        }

        public static List<UserSelect> GetAllVisibleUsersProfessions()
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("GetAllVisibleUsersProfessions", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(sqlDataReader);
                if (dt.Rows.Count == 0)
                    return null;
                return (from DataRow row in dt.Rows
                        select new UserSelect()
                        {
                            id = row["professiontypeid"].ToString(),
                            label = row["professionname"].ToString()
                        }).ToList();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return null;
        }

        public static void ChangeCulture(string culture)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("ChangeCulture", sqlConnection);

            UserData user = UserData.GetUserDataCookie();

            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = user.UserID;
            sqlCommand.Parameters.Add("@cultureName", SqlDbType.NVarChar, 5).Value = culture;
            sqlCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();

                user.CultureName = culture;
                string cookieName = ConfigurationManager.AppSettings["CookieName"];
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, user.Email, DateTime.Now, DateTime.Now.AddMinutes(30), true, new JavaScriptSerializer().Serialize(user), FormsAuthentication.FormsCookiePath);
                string encTicket = FormsAuthentication.Encrypt(ticket);
                HttpContext.Current.Response.Cookies.Add(new HttpCookie(cookieName, encTicket));
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public static void ChangePassword(int userID, string password)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("ChangePassword", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = userID;
            sqlCommand.Parameters.Add("@password", SqlDbType.NVarChar, 255).Value = EncodePassword(password);

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public static string SendPassword(string email)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("GetPasswordByEmail", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@returnValue", SqlDbType.Int, 0).Direction = ParameterDirection.ReturnValue;
            sqlCommand.Parameters.Add("@email", SqlDbType.VarChar).Value = email;

            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(sqlDataReader);
                if (int.Parse(sqlCommand.Parameters["@returnValue"].Value.ToString()) == 1)
                {
                    EmailQueue.AddEmail(new SendPasswordMail(email, DecodePassword(dt.Rows[0][0].ToString())));
                    return ResourceHelper.GetString("sPasswordSend");
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return ResourceHelper.GetString("sEmailNotExists");
        }
    }
}