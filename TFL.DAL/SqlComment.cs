﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for SqlComment
/// </summary>
namespace TFL.DAL
{
    public class SqlComment
    {
        public static FullComment GetCommentByParent(string parent)
        {
            List<Comment> list = new List<Comment>();
            UserData user = UserData.GetUserDataCookie();
            bool canDeleteRole = SqlRoles.CanAccess(user, 3);
            SqlParameter[] parameters = { new SqlParameter("@parentId", SqlDbType.VarChar) { Value = parent } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "GetCommentByParent");
            foreach (DataRow camp in cDataTable.Rows)
            {
                list.Add(new Comment()
                {
                    CommentId = int.Parse(camp["CommentId"].ToString()),
                    CommentString = camp["Comment"].ToString(),
                    CommentTime = Convert.ToDateTime(camp["CommentTime"].ToString()),
                    UserId = int.Parse(camp["userid"].ToString()),
                    UserName = camp["fullname"].ToString(),
                    ProfilePhoto = UrlHelper.GetProfileSmall(camp["userid"].ToString()),
                    CanDelete = canDeleteRole || (user != null && (user.UserID == int.Parse(camp["userid"].ToString())))
                });
            }
            return new FullComment()
            {
                Comments = list,
                IsLogin = (user != null),
                ProfilePath = (user != null ? UrlHelper.GetProfileSmall(user.UserID.ToString()) : UrlHelper.GetProfileSmall("None")),
                UserName = (user != null ? user.FirstName + " " + user.LastName : "")
            };
        }

        public static int AddCommentToParent(string parent, string comment)
        {
            UserData user = UserData.GetUserDataCookie();
            if (user == null)
                return -1;
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("CreateComment", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@returnValue", SqlDbType.Int, 0).Direction = ParameterDirection.ReturnValue;
            sqlCommand.Parameters.Add("@parentid", SqlDbType.VarChar).Value = parent;
            sqlCommand.Parameters.Add("@comment", SqlDbType.VarChar).Value = comment;
            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = user.UserID;
            sqlCommand.Parameters.Add("@userip", SqlDbType.VarChar).Value = IPClass.GetIPAddress();
            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteScalar();
                return int.Parse(sqlCommand.Parameters["@returnValue"].Value.ToString());
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public static void DeleteComment(int commentid)
        {
            UserData user = UserData.GetUserDataCookie();
            if (user == null)
                return;
            bool canDeleteRole = SqlRoles.CanAccess(user, 3);
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("DeleteComment", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@commentid", SqlDbType.Int).Value = commentid;
            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = (canDeleteRole ? -1 : user.UserID);

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }
        public static void UpdateComment(int commentid, string comment)
        {
            Thread.Sleep(1000);
            UserData user = UserData.GetUserDataCookie();
            if (user == null)
                return;
            bool canDeleteRole = SqlRoles.CanAccess(user, 3);
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("UpdateComment", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@commentid", SqlDbType.Int).Value = commentid;
            sqlCommand.Parameters.Add("@commentstring", SqlDbType.VarChar).Value = comment;
            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = (canDeleteRole ? -1 : user.UserID);

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }
    }
}