﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DataAccess
/// </summary>
namespace TFL.DAL
{
    public class DataAccess
    {
        public static DataTable GetDataTable(string connectionString, SqlParameter[] parameters, string storedProcedure)
        {
            SqlDataReader sqlDataReader = null;
            DataTable dt = new DataTable();
            try
            {
                SqlConnection sqlConnection = new SqlConnection(connectionString);
                SqlCommand sqlCommand = new SqlCommand(storedProcedure, sqlConnection);

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameters);
                sqlConnection.Open();
                sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                if (sqlDataReader.HasRows)
                {
                    //sqlDataReader.Read();
                    dt.Load(sqlDataReader);
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                if (sqlDataReader != null) { sqlDataReader.Close(); }
            }
            return dt;
        }
    }
}