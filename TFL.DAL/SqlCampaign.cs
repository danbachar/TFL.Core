﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;
using TFL.BO;

/// <summary>
/// Summary description for SqlCampaign
/// </summary>
namespace TFL.DAL
{
    public class SqlCampaign
    {
        public static List<Campaign> GetLastCampaigns()
        {
            List<Campaign> list = new List<Campaign>();
            SqlParameter[] parameters = { new SqlParameter("@NumOfCampaigns", SqlDbType.Int) { Value = 6 } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "GetLastCampaigns");
            foreach (DataRow camp in cDataTable.Rows)
            {
                list.Add(new Campaign()
                {
                    CampaignID = (long)camp["campaignID"],
                    CampaignName = camp["campaignName"].ToString(),
                    CampaignPublished = Convert.ToDateTime(camp["campaignPublished"].ToString()),
                    CampaignTypeName = ResourceHelper.GetString(camp["campaignTypeName"].ToString()).ToString(),
                    CampaignDescription = camp["campaignDescription"].ToString(),
                    CampaignAlbumCover = UrlHelper.CampaignUploads(camp["campaignAlbumCover"].ToString()),
                    ClientName = camp["clientname"].ToString(),
                    ClientPath = MemberPath.GetMemberPath(Convert.ToInt32(camp["clientuserid"].ToString()), camp["clientfirstname"].ToString(), camp["clientlastname"].ToString(), 3)
                });
            }
            return list;
        }

        public static List<Campaign> GetCampaignByTypeID(int typeid, int lastseen)
        {
            List<Campaign> list = new List<Campaign>();
            SqlParameter[] parameters = { new SqlParameter("@typeid", SqlDbType.Int) { Value = typeid }, new SqlParameter("@lastseen", SqlDbType.Int) { Value = lastseen } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "GetCampaignByTypeID");
            foreach (DataRow camp in cDataTable.Rows)
            {
                list.Add(new Campaign()
                {
                    CampaignID = (long)camp["campaignID"],
                    CampaignName = camp["campaignName"].ToString(),
                    CampaignPublished = Convert.ToDateTime(camp["campaignPublished"].ToString()),
                    CampaignDatePublishText = Convert.ToDateTime(camp["campaignPublished"].ToString()).ToString("dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture),
                    CampaignTypeName = ResourceHelper.GetString(camp["campaignTypeName"].ToString()).ToString(),
                    CampaignDescription = camp["campaignDescription"].ToString(),
                    CampaignAlbumCover = UrlHelper.CampaignUploads(camp["campaignAlbumCover"].ToString()),
                    ClientName = camp["clientname"].ToString(),
                    ClientPath = MemberPath.GetMemberPath(Convert.ToInt32(camp["clientuserid"].ToString()), camp["clientfirstname"].ToString(), camp["clientlastname"].ToString(), 3)
                });
            }
            return list;
        }

        public static int SaveCampaign(FullCampaign campaign)
        {
            ConvertTempFilesToCampaignFiles(campaign.Files);
            campaign.Tags = AddingNewTags(campaign.Tags);
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("CreateCampaign", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            //sqlCommand.Parameters.Add("@returnValue", SqlDbType.Int, 0).Direction = ParameterDirection.ReturnValue;

            sqlCommand.Parameters.Add("@campaignDefaultID", SqlDbType.Int).Value = campaign.CampaignID;
            sqlCommand.Parameters.Add("@campaginName", SqlDbType.VarChar, 255).Value = campaign.CampaignName;
            sqlCommand.Parameters.Add("@creator", SqlDbType.VarChar).Value = campaign.Creator;
            sqlCommand.Parameters.Add("@campaginPublished", SqlDbType.Date).Value = campaign.CampaignPublished;
            sqlCommand.Parameters.Add("@campaginTypeid", SqlDbType.Int).Value = campaign.CampaignTypeID;
            sqlCommand.Parameters.Add("@campaginDescription", SqlDbType.VarChar, -1).Value = campaign.CampaignDescription;
            sqlCommand.Parameters.Add("@campaginCover", SqlDbType.VarChar, 255).Value = GetCover(campaign.Files);
            sqlCommand.Parameters.Add("@campaginProfessions", SqlDbType.VarChar, -1).Value = campaign.Users + ",";
            sqlCommand.Parameters.Add("@campaginTags", SqlDbType.VarChar, -1).Value = campaign.Tags;
            sqlCommand.Parameters.Add("@campaginVideos", SqlDbType.VarChar, -1).Value = (campaign.Videos != "" ? campaign.Videos + "," : "");
            sqlCommand.Parameters.Add("@isapproved", SqlDbType.Int).Value = campaign.IsPublish;
            sqlCommand.Parameters.Add("@filesTable", SqlDbType.Structured).Value = FullCampaign.GetDataTableFiles(campaign.Files);
            sqlCommand.Parameters.Add("@userCreate", SqlDbType.Int).Value = UserData.GetUserDataCookie().UserID;
            sqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int, 0).Direction = ParameterDirection.Output;

            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                return Convert.ToInt32(sqlCommand.Parameters["@CampaignID"].Value);
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
                ChangeSessionID();
            }
            return -1;
        }

        public static string AddingNewTags(string tags)
        {
            List<string> tagstring = new List<string>();
            if (!string.IsNullOrEmpty(tags))
            {
                foreach (string tag in tags.Split(','))
                {
                    if (tag.StartsWith("t"))
                        tagstring.Add(tag.Replace("t", ""));
                    else if (tag.StartsWith(":-"))
                        tagstring.Add(TagSelect.AddTag(tag.Substring(2)).ToString());
                    else
                        tagstring.Add(tag);
                }
                return string.Join(",", tagstring) + ",";
            }
            else
            {
                return tags;
            }
        }

        private static void ConvertTempFilesToCampaignFiles(List<CampaignFiles> files)
        {
            string key = HttpContext.Current.Session.SessionID;
            string moreGuid = Guid.NewGuid().ToString();
            string temppath = UrlHelper.StaticUrlPath("~/TempFiles");
            string campaignpath = UrlHelper.StaticUrlPath("~/CampaignsUpload");
            foreach (CampaignFiles file in files)
            {
                try
                {
                    string fromPath = Path.Combine(temppath, key + "_" + file.FileName);
                    string ext = GetExtantion(temppath, key + "_" + file.FileName);
                    fromPath = fromPath + ext;
                    string toPath = Path.Combine(campaignpath, file.FileName + "-" + moreGuid + ext);
                    while (File.Exists(toPath))
                    {
                        moreGuid = Guid.NewGuid().ToString();
                        toPath = Path.Combine(campaignpath, file.FileName + "-" + moreGuid + ext);
                    }
                    File.Move(fromPath, toPath);
                    file.FileName = file.FileName + "-" + moreGuid + ext;
                    if (file.SetAsCover)
                        ImageHelper.SaveThumb(toPath, 170, 170);
                    else
                        ImageHelper.SaveSmallThumb(toPath, 100, 100);
                }
                catch (Exception)
                {
                    if (file.SetAsCover)
                        ImageHelper.SaveThumb(Path.Combine(campaignpath, file.FileName), 170, 170);
                    else
                        ImageHelper.SaveSmallThumb(Path.Combine(campaignpath, file.FileName), 100, 100);
                }
            }

        }

        private static string GetExtantion(string temppath, string filename)
        {
            DirectoryInfo root = new DirectoryInfo(temppath);
            FileInfo[] listfiles = root.GetFiles(filename + ".*");
            return listfiles[0].Extension;
        }

        private static string GetCover(List<CampaignFiles> files)
        {
            foreach (CampaignFiles file in files)
            {
                if (file.SetAsCover)
                    return ImageHelper.GetCoverPath(file.FileName);
            }
            return "";
        }

        public static FullCampaign GetCampaignByID(int id)
        {
            string countryid = LanguageHelper.GetCountry("-");
            FullCampaign campaign = new FullCampaign();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("GetCampaignByID", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int, 0).Value = id;
            sqlCommand.Parameters.Add("@countryid", SqlDbType.VarChar).Value = countryid;
            sqlCommand.Parameters.Add("@culture", SqlDbType.VarChar).Value = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;

            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader;
                DataTable dt = new DataTable();
                sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                //campaign Details
                dt.Load(sqlDataReader);
                if (dt.Rows.Count == 0)
                    throw new Exception("CampaignNotFound");


                campaign.CampaignName = dt.Rows[0]["campaignName"].ToString();
                campaign.CampaignPublished = Convert.ToDateTime(dt.Rows[0]["campaignPublished"].ToString());
                campaign.CampaignTypeID = Convert.ToInt32(dt.Rows[0]["campaignTypeId"].ToString());
                campaign.CampaignDescription = dt.Rows[0]["campaignDescription"].ToString().Replace("\"", "").Replace("'", "\'").Replace("\"", "'").Replace('\n', ' ');
                campaign.CampaignAlbumCover = UrlHelper.StaticUrl("CampaignsUpload/" + dt.Rows[0]["campaignAlbumCover"].ToString());
                campaign.AuthorName = dt.Rows[0]["fullname"].ToString();
                campaign.AuthorID = Convert.ToInt32(dt.Rows[0]["userid"].ToString());
                campaign.CampaignTypeName = dt.Rows[0]["campaignTypeName"].ToString();
                campaign.ClientPath = MemberPath.GetMemberPath(Convert.ToInt32(dt.Rows[0]["createuserid"].ToString()), dt.Rows[0]["createfirstname"].ToString(), dt.Rows[0]["createlastname"].ToString(), Convert.ToInt32(dt.Rows[0]["createtypeid"].ToString()));
                campaign.AuthorType = Convert.ToInt32(dt.Rows[0]["professionTypeId"].ToString());

                //Users
                dt = new DataTable();
                dt.Load(sqlDataReader);
                List<UserSelect> users = (from DataRow row in dt.Rows
                                          select new UserSelect()
                                          {
                                              label = row["full_name"].ToString(),
                                              id = row["professionUserId"].ToString(),
                                              name = row["professionTypeId"].ToString()
                                          }).ToList();
                campaign.UsersList = users;


                //Tags
                dt = new DataTable();
                dt.Load(sqlDataReader);
                List<TagSelect> tags = (from DataRow row in dt.Rows
                                        select new TagSelect()
                                        {
                                            id = row["tagid"].ToString(),
                                            name = row["tagname"].ToString()
                                        }).ToList();
                campaign.TagsList = tags;

                //Videos
                dt = new DataTable();
                dt.Load(sqlDataReader);
                List<string> videos = (from DataRow row in dt.Rows
                                       select row["videoid"].ToString()).ToList();
                campaign.Videos = string.Join(",", videos);


                //Files
                dt = new DataTable();
                dt.Load(sqlDataReader);
                List<CampaignFiles> files = (from DataRow row in dt.Rows
                                             select new CampaignFiles()
                                             {
                                                 Place = Convert.ToInt32(row["fileid"].ToString().ToString()),
                                                 FileName = UrlHelper.CampaignUploads(row["filename"].ToString()),
                                                 Description = row["description"].ToString(),
                                                 SetAsCover = campaign.CampaignAlbumCover.Replace("_t", "") == row["filename"].ToString(),
                                                 SetAsFirst = row["isfirst"].ToString() == "True",
                                                 IsNude = row["isnude"].ToString() == "True",
                                                 Tags = string.Join(",", row["tags"].ToString().Split(',').Select(x => x + "|" + ResourceHelper.GetTagByID(x.ToString())))
                                             }).ToList();
                campaign.Files = files.OrderByDescending(x => x.SetAsFirst).ThenBy(x => x.Place).ToList();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return campaign;
        }

        private static void ChangeSessionID()
        {
            SessionIDManager Manager = new SessionIDManager();

            string NewID = Manager.CreateSessionID(HttpContext.Current);
            bool redirected = false;
            bool IsAdded = false;
            Manager.SaveSessionID(HttpContext.Current, NewID, out redirected, out IsAdded);
        }

        public static List<Campaign> GetCampaignByTypeIDAndUserID(int typeid, int lastread, int userid)
        {
            List<Campaign> list = new List<Campaign>();
            SqlParameter[] parameters = { new SqlParameter("@typeid", SqlDbType.Int) { Value = typeid }, new SqlParameter("@userid", SqlDbType.Int) { Value = userid }, new SqlParameter("@lastread", SqlDbType.Int) { Value = lastread } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "GetCampaignByTypeIDAndUserID");
            foreach (DataRow camp in cDataTable.Rows)
            {
                list.Add(new Campaign()
                {
                    CampaignID = (long)camp["campaignID"],
                    CampaignName = camp["campaignName"].ToString(),
                    CampaignPublished = Convert.ToDateTime(camp["campaignPublished"].ToString()),
                    CampaignDatePublishText = Convert.ToDateTime(camp["campaignPublished"].ToString()).ToString("dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture),
                    CampaignTypeName = ResourceHelper.GetString(camp["campaignTypeName"].ToString()).ToString(),
                    CampaignDescription = camp["campaignDescription"].ToString(),
                    CampaignAlbumCover = UrlHelper.CampaignUploads(camp["campaignAlbumCover"].ToString()),
                    ClientName = camp["clientname"].ToString(),
                    ClientPath = MemberPath.GetMemberPath(Convert.ToInt32(camp["userid"].ToString()), camp["firstname"].ToString(), camp["lastname"].ToString(), 3)
                });
            }
            return list;
        }


        public static List<Campaign> SearchCampaigns(Campaign campaign)
        {
            List<Campaign> list = new List<Campaign>();
            SqlParameter[] parameters =
            {
             new SqlParameter("@campaignName", SqlDbType.VarChar) { Value = campaign.CampaignName },
             new SqlParameter("@campaignPublished", SqlDbType.Date) { Value = (campaign.CampaignDatePublishText==""?(DateTime?)null:Convert.ToDateTime(campaign.CampaignDatePublishText)) },
             new SqlParameter("@campaignTypeId", SqlDbType.Int) { Value = campaign.CampaignTypeID },
             new SqlParameter("@campaignDescription", SqlDbType.VarChar) { Value = campaign.CampaignDescription },
             new SqlParameter("@nudeMode", SqlDbType.Int) { Value = campaign.NudeMode },
        };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "SearchCampaigns");
            foreach (DataRow camp in cDataTable.Rows)
            {
                list.Add(new Campaign()
                {
                    CampaignID = (long)camp["campaignID"],
                    CampaignName = camp["campaignName"].ToString(),
                    CampaignPublished = Convert.ToDateTime(camp["campaignPublished"].ToString()),
                    CampaignDatePublishText = Convert.ToDateTime(camp["campaignPublished"].ToString()).ToString("dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture),
                    CampaignTypeName = ResourceHelper.GetString(camp["campaignTypeName"].ToString()).ToString(),
                    CampaignDescription = Homepage.ArticleShortDescription(camp["campaignDescription"].ToString(), 100),
                    CampaignAlbumCover = UrlHelper.CampaignUploads(camp["campaignAlbumCover"].ToString()),
                    ClientName = camp["clientname"].ToString(),
                    ClientPath = MemberPath.GetMemberPath(Convert.ToInt32(camp["clientuserid"].ToString()), camp["clientfirstname"].ToString(), camp["clientlastname"].ToString(), 3)
                });
            }
            return list;
        }

        public static void DeleteCampaign(int id)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("DeleteCampaign", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int).Value = id;

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public static void ChangeCampaignStatus(int campaignid, int status)
        {
            if (UserData.GetUserDataCookie().UserType != UserData.UserRole.Admin)
                return;
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("ChangeCampaignStatus", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@campaignid", SqlDbType.Int).Value = campaignid;
            sqlCommand.Parameters.Add("@statusid", SqlDbType.Int, 255).Value = status;

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
                ChangeSessionID();
            }
        }
    }
}