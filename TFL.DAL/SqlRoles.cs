﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for SqlRoles
/// </summary>
namespace TFL.DAL
{
    public class SqlRoles
    {
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public string RoleType { get; set; }
        public string RoleCode { get; set; }

        public static bool CanAccess(UserData user, int requiredPriv)
        {
            if (user == null)
                return false;
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("GetPrivByRole", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@RoleID", SqlDbType.Int).Value = user.UserType;
            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(sqlDataReader);
                if (dt.Rows.Count == 0)
                    return false;
                if (dt.Rows.Cast<DataRow>().Any(row => int.Parse(row["PrivId"].ToString()) == requiredPriv))
                {
                    return true;
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return false;
        }

        public static DataTable GetPrivRoles()
        {
            DataTable dt = new DataTable();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("GetRolesPrivs", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                dt.Load(sqlDataReader);
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            DataTable roles = new DataTable();
            roles.Columns.Add(" ");
            List<string> values = new List<string>() { ResourceHelper.GetString(dt.Rows[0]["ProfessionName"].ToString()).ToString() + "|" + dt.Rows[0]["ProfessionTypeid"] };
            int i = 0;
            int max = 0;
            foreach (DataRow row in dt.Rows)
            {
                string colname = ResourceHelper.GetString(row["PrivName"].ToString()).ToString();
                if (roles.Columns.IndexOf(colname) == -1)
                {
                    roles.Columns.Add(colname);
                    values.Add(row["haspriv"].ToString());
                    max++;
                }
                else
                {
                    if (i == max)
                    {
                        roles.Rows.Add(string.Join(",", values).Split(','));
                        values = new List<string>() { ResourceHelper.GetString(row["ProfessionName"].ToString()).ToString() + "|" + row["ProfessionTypeid"] };
                        i = 0;
                    }
                    values.Add(row["haspriv"].ToString());
                }
                i++;
            }
            if (i == max)
                roles.Rows.Add(string.Join(",", values).Split(','));
            return roles;
        }

        public static void SetPrivRoles(List<List<bool>> rolesPriv)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("SetRolesPrivs", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@rolesTable", SqlDbType.Structured).Value = convertToDataTable(rolesPriv);
            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        private static DataTable convertToDataTable(List<List<bool>> rolesPriv)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("RoleID"));
            dt.Columns.Add(new DataColumn("PrivId"));
            for (int i = 0; i < rolesPriv.Count; i++)
            {
                for (int j = 0; j < rolesPriv[i].Count; j++)
                {
                    if (rolesPriv[i][j])
                    {
                        dt.Rows.Add(i, j + 1);
                    }
                }
            }
            return dt;
        }

        public static void AddRole(string rolename, int typeid)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            string rolecode = "s" + textInfo.ToTitleCase(rolename.ToLower()).Replace(" ", "");

            Dictionary<string, string> dictionary = ResourceHelper.GetDictionaryResource();

            int index = 1;
            while (dictionary.ContainsKey(rolecode))
            {
                rolecode += index;
                index++;
            }


            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("CreateRole", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@rolecode", SqlDbType.VarChar).Value = rolecode;
            sqlCommand.Parameters.Add("@rank", SqlDbType.Int).Value = typeid;

            sqlCommand.Parameters.Add("@culture", SqlDbType.VarChar).Value = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            sqlCommand.Parameters.Add("@rolename", SqlDbType.VarChar).Value = rolename;

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            ResourceHelper.ClearCache();
        }

        public static string GetAllRoles()
        {
            List<SqlRoles> list = new List<SqlRoles>();

            DataTable dt = new DataTable();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("GetAllUsersProfessions", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                dt.Load(sqlDataReader);

                foreach (DataRow row in dt.Rows)
                {
                    list.Add(new SqlRoles()
                    {
                        RoleID = Convert.ToInt32(row["ProfessionTypeId"].ToString()),
                        RoleName = ResourceHelper.GetString(row["ProfessionName"].ToString()).ToString(),
                        RoleType = GetRolesTypes().FirstOrDefault(x => x.Value == row["rank"].ToString()).Text,
                        RoleCode = row["ProfessionName"].ToString()
                    });
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return new JavaScriptSerializer().Serialize(list);
        }

        public static List<ListItem> GetRolesTypes()
        {
            List<ListItem> list = new List<ListItem>();
            list.Add(new ListItem(ResourceHelper.GetString("sManagmentRoles").ToString(), "0"));
            list.Add(new ListItem(ResourceHelper.GetString("sProfessionalRoles").ToString(), "1"));
            list.Add(new ListItem(ResourceHelper.GetString("sOtherProfessionalRoles").ToString(), "2"));
            return list;
        }

        public static void DeleteSelected(TagSelect[] id)
        {
            foreach (TagSelect tagSelect in id)
            {
                SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
                SqlCommand sqlCommand = new SqlCommand("DeleteRole", sqlConnection);

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add("@roleid", SqlDbType.Int).Value = Convert.ToInt32(tagSelect.id);
                sqlCommand.Parameters.Add("@rolename", SqlDbType.VarChar).Value = tagSelect.name;
                try
                {
                    sqlConnection.Open();
                    sqlCommand.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    throw e;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }
    }
}