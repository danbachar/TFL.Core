﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for SqlMember
/// </summary>
namespace TFL.DAL
{
    public class SqlMember
    {
        public static Member GetMember(int typeid, string userpath)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            UserData myuser = UserData.GetUserDataCookie();

            Member member = new Member();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("GetMemberDetails", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@typeid", SqlDbType.Int).Value = typeid;
            sqlCommand.Parameters.Add("@userpath", SqlDbType.VarChar).Value = userpath;
            sqlCommand.Parameters.Add("@culture", SqlDbType.VarChar).Value = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;

            sqlCommand.Parameters.Add("@role", SqlDbType.Int).Value = (myuser == null ? -1 : (int)myuser.UserType);
            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader;
                DataTable dt = new DataTable();
                sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                //member Details
                dt.Load(sqlDataReader);
                if (dt.Rows.Count == 0)
                    throw new Exception("UserNotFound");

                member.UserId = Convert.ToInt32(dt.Rows[0]["userid"].ToString());

                member.FirstName = dt.Rows[0]["firstname"].ToString();
                member.LastName = dt.Rows[0]["lastname"].ToString();
                member.UserType = Convert.ToInt32(dt.Rows[0]["professionTypeId"].ToString());
                member.ProfessionName = dt.Rows[0]["professionName"].ToString();
                member.Gender = Convert.ToBoolean(dt.Rows[0]["Gender"]);
                member.BirthDate = (dt.Rows[0]["birthdate"].ToString() != "" ? Convert.ToDateTime(dt.Rows[0]["birthdate"].ToString(), System.Threading.Thread.CurrentThread.CurrentCulture) : (DateTime?)null);
                member.Biography = dt.Rows[0]["biography"].ToString();
                member.Website = dt.Rows[0]["website"].ToString();
                member.Profile = UrlHelper.GetProfileMedium(dt.Rows[0]["userid"].ToString());
                member.CountryID = dt.Rows[0]["countryid"].ToString() != "" ? textInfo.ToTitleCase(dt.Rows[0]["countryid"].ToString().ToLower()) : "";
                member.CityID = dt.Rows[0]["cityid"].ToString() != "" ? textInfo.ToTitleCase(dt.Rows[0]["cityid"].ToString().ToLower()) : "";
                try
                {


                    ModelProperty properties = new ModelProperty()
                    {
                        Hair = dt.Rows[0]["hairid"].ToString(),
                        Eyes = dt.Rows[0]["eyesid"].ToString(),
                        Bust = Convert.ToDecimal(dt.Rows[0]["bust"].ToString()),
                        Height = Convert.ToDecimal(dt.Rows[0]["height"].ToString()),
                        Waist = Convert.ToDecimal(dt.Rows[0]["waist"].ToString()),
                        Hipe = Convert.ToDecimal(dt.Rows[0]["hips"].ToString()),
                        Shoe = Convert.ToDecimal(dt.Rows[0]["shoe"].ToString())
                    };

                    member.ModelProperty = properties;
                }
                catch
                {
                    //Not Have Properties
                }


                //Has Campaigns
                dt = new DataTable();
                dt.Load(sqlDataReader);
                member.HasCampaigns = int.Parse(dt.Rows[0][0].ToString()) > 0;


                //Has News
                dt = new DataTable();
                dt.Load(sqlDataReader);
                member.HasNews = int.Parse(dt.Rows[0][0].ToString()) > 0;

                if (member.UserType == 14)
                {
                    //Has Models
                    dt = new DataTable();
                    dt.Load(sqlDataReader);
                    member.HasModels = int.Parse(dt.Rows[0][0].ToString()) > 0;
                }

            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return member;
        }

        public static Author GetAuthor(string userpath)
        {
            Author member = new Author();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("GetAuthorById", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@userpath", SqlDbType.VarChar).Value = userpath;

            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader;
                DataTable dt = new DataTable();
                sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                //Author Details
                dt.Load(sqlDataReader);
                if (dt.Rows.Count == 0)
                    throw new Exception("UserNotFound");

                member.Userid = Convert.ToInt32(dt.Rows[0]["userid"].ToString());

                member.FirstName = dt.Rows[0]["firstname"].ToString();
                member.LastName = dt.Rows[0]["lastname"].ToString();
                member.Profile = UrlHelper.GetProfileMedium(dt.Rows[0]["userid"].ToString());

                dt = new DataTable();
                dt.Load(sqlDataReader);
                member.CountOfArticles = Convert.ToInt32(dt.Rows[0][0].ToString());
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return member;
        }

        public static List<Member> SearchMembers(Member member)
        {
            string culture = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            TextInfo textInfo = Thread.CurrentThread.CurrentCulture.TextInfo;

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("SearchMembers", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@firstname", SqlDbType.VarChar).Value = member.FirstName;
            sqlCommand.Parameters.Add("@lastname", SqlDbType.VarChar).Value = member.LastName;
            sqlCommand.Parameters.Add("@typeid", SqlDbType.Int).Value = member.UserType;
            sqlCommand.Parameters.Add("@gender", SqlDbType.Int).Value = member.GenderSelect;
            sqlCommand.Parameters.Add("@birthdate", SqlDbType.Date).Value = (member.BirthDateString != "" ? Convert.ToDateTime(member.BirthDateString, System.Threading.Thread.CurrentThread.CurrentCulture) : (DateTime?)null);
            sqlCommand.Parameters.Add("@countryid", SqlDbType.VarChar).Value = member.CountryID;
            sqlCommand.Parameters.Add("@cityid", SqlDbType.VarChar).Value = member.CityID;
            sqlCommand.Parameters.Add("@email", SqlDbType.VarChar).Value = member.Email;
            sqlCommand.Parameters.Add("@website", SqlDbType.VarChar).Value = member.Website;
            sqlCommand.Parameters.Add("@biography", SqlDbType.VarChar).Value = member.Biography;

            sqlCommand.Parameters.Add("@hair", SqlDbType.VarChar);
            sqlCommand.Parameters.Add("@eyesid", SqlDbType.VarChar);
            sqlCommand.Parameters.Add("@bustfrom", SqlDbType.Decimal);
            sqlCommand.Parameters.Add("@bustto", SqlDbType.Decimal);
            sqlCommand.Parameters.Add("@heightfrom", SqlDbType.Decimal);
            sqlCommand.Parameters.Add("@heightto", SqlDbType.Decimal);
            sqlCommand.Parameters.Add("@waistfrom", SqlDbType.Decimal);
            sqlCommand.Parameters.Add("@waistto", SqlDbType.Decimal);
            sqlCommand.Parameters.Add("@hipsfrom", SqlDbType.Decimal);
            sqlCommand.Parameters.Add("@hipsto", SqlDbType.Decimal);
            sqlCommand.Parameters.Add("@shoefrom", SqlDbType.VarChar);
            sqlCommand.Parameters.Add("@shoeto", SqlDbType.VarChar);
            sqlCommand.Parameters.Add("@nudeMode", SqlDbType.Int).Value = member.NudeMode;
            if (member.UserType == 5 && member.ModelProperty != null)
            {
                sqlCommand.Parameters["@hair"].Value = member.ModelProperty.Hair;
                sqlCommand.Parameters["@eyesid"].Value = member.ModelProperty.Eyes;
                sqlCommand.Parameters["@bustfrom"].Value = member.ModelProperty.Bust;
                sqlCommand.Parameters["@bustto"].Value = member.ModelProperty.BustTo;
                sqlCommand.Parameters["@heightfrom"].Value = member.ModelProperty.Height;
                sqlCommand.Parameters["@heightto"].Value = member.ModelProperty.HeightTo;
                sqlCommand.Parameters["@waistfrom"].Value = member.ModelProperty.Waist;
                sqlCommand.Parameters["@waistto"].Value = member.ModelProperty.WaistTo;
                sqlCommand.Parameters["@hipsfrom"].Value = member.ModelProperty.Hipe;
                sqlCommand.Parameters["@hipsto"].Value = member.ModelProperty.HipeTo;
                sqlCommand.Parameters["@shoefrom"].Value = member.ModelProperty.Shoe;
                sqlCommand.Parameters["@shoeto"].Value = member.ModelProperty.ShoeTo;
            }

            sqlCommand.Parameters.Add("@culture", SqlDbType.VarChar).Value = culture;

            DataTable dt = new DataTable();

            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader;
                sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                //Author Details
                dt.Load(sqlDataReader);
                List<Member> list = new List<Member>();
                foreach (DataRow row in dt.Rows)
                {
                    Member m = new Member()
                    {
                        UserId = Convert.ToInt32(row["userid"].ToString()),
                        FirstName = row["firstname"].ToString(),
                        LastName = row["lastname"].ToString(),
                        FullName = row["fullname"].ToString(),
                        GenderName = row["gender"].ToString() == "True" ? ResourceHelper.GetString("sMale").ToString() : ResourceHelper.GetString("sFemale").ToString(),
                        GenderSelect = row["gender"].ToString() == "True" ? 1 : 0,
                        BirthDate = row["birthdate"].ToString() != "" ? Convert.ToDateTime(row["birthdate"].ToString(), System.Threading.Thread.CurrentThread.CurrentCulture) : (DateTime?)null,
                        BirthDateString = row["birthdate"].ToString() != "" ? Convert.ToDateTime(row["birthdate"].ToString(), System.Threading.Thread.CurrentThread.CurrentCulture).ToShortDateString() : "",
                        Biography = row["biography"].ToString(),
                        Website = row["website"].ToString(),
                        Email = row["email"].ToString(),
                        ProfessionName = (row["ProfessionName"].ToString() != "" ? ResourceHelper.GetString(row["ProfessionName"].ToString()).ToString() : ""),
                        UserType = (row["professionTypeId"].ToString() != "" ? Convert.ToInt32(row["professionTypeId"]) : -1),
                        Profile = UrlHelper.GetProfileSmall(row["userid"].ToString()),
                        UserPath = "../" + MemberPath.GetMemberPath(Convert.ToInt32(row["userid"].ToString()), row["firstname"].ToString(), row["lastname"].ToString(), (row["professionTypeId"].ToString() != "" ? Convert.ToInt32(row["professionTypeId"]) : -1)),
                        LastLoginDate = row["LastLoginDate"].ToString() != "" ? Convert.ToDateTime(row["LastLoginDate"].ToString(), Thread.CurrentThread.CurrentCulture).ToString(Thread.CurrentThread.CurrentCulture) : "",
                        IsLocked = Convert.ToBoolean(row["IsLockedOut"].ToString()),
                        LastLockedDate = row["LastLockoutDate"].ToString() != "" ? Convert.ToDateTime(row["LastLockoutDate"].ToString(), Thread.CurrentThread.CurrentCulture).ToString(Thread.CurrentThread.CurrentCulture) : "",
                        CountryID = textInfo.ToTitleCase(row["countryname"].ToString().ToLower()),
                        CityID = textInfo.ToTitleCase(row["cityname"].ToString().ToLower()),
                        AgreeForMails = Convert.ToBoolean(row["agreeForMails"].ToString()),
                        CreateDate = row["CreateDate"].ToString() != "" ? Convert.ToDateTime(row["CreateDate"].ToString(), Thread.CurrentThread.CurrentCulture).ToString(Thread.CurrentThread.CurrentCulture) : "",
                    };
                    ModelProperty mp = new ModelProperty()
                    {
                        Hair = row["hairid"].ToString(),
                        Eyes = row["eyesid"].ToString(),
                        Bust = row["bust"].ToString() == "" ? -1 : Convert.ToDecimal(row["bust"].ToString()),
                        Height = row["height"].ToString() == "" ? -1 : Convert.ToDecimal(row["height"].ToString()),
                        Waist = row["waist"].ToString() == "" ? -1 : Convert.ToDecimal(row["waist"].ToString()),
                        Hipe = row["hips"].ToString() == "" ? -1 : Convert.ToDecimal(row["hips"].ToString()),
                        Shoe = row["shoe"].ToString() == "" ? -1 : Convert.ToDecimal(row["shoe"].ToString())
                    };

                    if (m.UserType == 5)
                        m.ModelProperty = mp;
                    list.Add(m);
                }
                return list;
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return null;
        }

        public static void UpdateProfession(int userid, int roleid)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("SetUserProfession", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
            sqlCommand.Parameters.Add("@professionid", SqlDbType.Int).Value = roleid;

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public static void UpdateWebSite(int userid, string website)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("UpdateWebsite", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
            sqlCommand.Parameters.Add("@website", SqlDbType.VarChar).Value = website;

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public static void UpdateBiography(int userid, string bio)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("UpdateBiography", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
            sqlCommand.Parameters.Add("@bio", SqlDbType.VarChar).Value = bio;

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }
        public static void UpdateBirthDateAndLocation(int userid, DateTime? birthdate, string countryid, string cityid)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("UpdateBirthDateAndLocation", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
            sqlCommand.Parameters.Add("@birthdate", SqlDbType.Date).Value = birthdate;
            sqlCommand.Parameters.Add("@countryid", SqlDbType.VarChar).Value = countryid;
            sqlCommand.Parameters.Add("@cityid", SqlDbType.VarChar).Value = cityid;

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public static void UpdateModelProperties(int userid, string hair, string eyesid, decimal bust, decimal height, decimal waist, decimal hips, int shoe)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("UpdateModelProperties", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
            sqlCommand.Parameters.Add("@hair", SqlDbType.VarChar).Value = hair;
            sqlCommand.Parameters.Add("@eyesid", SqlDbType.VarChar).Value = eyesid;
            sqlCommand.Parameters.Add("@bust", SqlDbType.Decimal).Value = bust;
            sqlCommand.Parameters.Add("@height", SqlDbType.Decimal).Value = height;
            sqlCommand.Parameters.Add("@waist", SqlDbType.Decimal).Value = waist;
            sqlCommand.Parameters.Add("@hips", SqlDbType.Decimal).Value = hips;
            sqlCommand.Parameters.Add("@shoe", SqlDbType.Int).Value = shoe;

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public static void LockOrUnlockUser(int userid)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("LockOrUnlockUser", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public static void DeleteMember(int userid)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("DeleteMember", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public static void AddRemoveNewsLetter(int userid)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("AddRemoveNewsLetter", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public static List<Agency> GetModelAgencies(int userid)
        {
            string culture = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            List<Agency> list = new List<Agency>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("GetModelAgencies", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
            sqlCommand.Parameters.Add("@culture", SqlDbType.VarChar).Value = culture;

            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader;
                sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                DataTable dt = new DataTable();
                dt.Load(sqlDataReader);

                foreach (DataRow row in dt.Rows)
                {
                    list.Add(new Agency()
                    {
                        AgencyLocation = (row["countryname"].ToString() == "") ? "" : string.Format("{1}, {0}", Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(row["countryname"].ToString().ToLower()), Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(row["cityname"].ToString().ToLower())),
                        AgencyLocationID = row["countryid"] + ";" + row["cityid"],
                        AgencyId = Convert.ToInt32(row["userid"].ToString()),
                        AgencyName = row["agencyname"].ToString()
                    });
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return list;
        }

        public static List<Member> GetMembersByTypeID(int typeid, int lastseen)
        {
            List<Member> list = new List<Member>();

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("GetMembersByTypeID", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@typeid", SqlDbType.Int).Value = typeid;
            sqlCommand.Parameters.Add("@lastseen", SqlDbType.Int).Value = lastseen;

            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader;
                sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                DataTable dt = new DataTable();
                dt.Load(sqlDataReader);

                foreach (DataRow row in dt.Rows)
                {
                    list.Add(new Member()
                    {
                        FirstName = row["firstname"].ToString(),
                        LastName = row["lastname"].ToString(),
                        UserPath = MemberPath.GetMemberPath(Convert.ToInt32(row["userid"].ToString()), row["firstname"].ToString(), row["lastname"].ToString(), Convert.ToInt32(row["professionTypeId"])),
                        ProfessionName = ResourceHelper.GetString(row["ProfessionName"].ToString()),
                        Profile = UrlHelper.GetProfileMedium(row["userid"].ToString())
                    });
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            return list;
        }
    }
}