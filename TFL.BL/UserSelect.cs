﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TFL_DAL;
using TFL.BO;

/// <summary>
/// Summary description for UserSelect
/// </summary>
namespace TFL_BL
{
    public class UserSelect
    {
        public static List<TFL.BO.UserSelect> GetUsersByKey(string key, int professionType)
        {
            return SqlUserSelect.GetUserSelect(key, professionType);
        }
    }
}