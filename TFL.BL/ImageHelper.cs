﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;

/// <summary>
/// Summary description for ImageHelper
/// </summary>
public class ImageHelper
{
    public static Image ResizeImage(Image image, Size size, bool preserveAspectRatio = true)
    {
        int newWidth;
        int newHeight;
        if (preserveAspectRatio)
        {
            int originalWidth = image.Width;
            int originalHeight = image.Height;
            float percentWidth = (float)size.Width / (float)originalWidth;
            float percentHeight = (float)size.Height / (float)originalHeight;
            float percent = percentHeight > percentWidth ? percentHeight : percentWidth;
            newWidth = (int)(originalWidth * percent);
            newHeight = (int)(originalHeight * percent);
        }
        else
        {
            newWidth = size.Width;
            newHeight = size.Height;
        }
        Image newImage = new Bitmap(newWidth, newHeight);
        using (Graphics graphicsHandle = Graphics.FromImage(newImage))
        {
            graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
        }
        return newImage;
    }

    public static void SaveThumb(string filepath, int width, int heigth)
    {
        if (!File.Exists(GetCoverPath(filepath)))
        {
            Image original = Image.FromFile(filepath);
            Image resized = ResizeImage(original, new Size(width, heigth));
            resized.Save(GetCoverPath(filepath));
        }
    }

    public static void SaveSmallThumb(string filepath, int width, int heigth)
    {
        if (!File.Exists(GetTumbPath(filepath)))
        {
            Image original = Image.FromFile(filepath);
            Image resized = ResizeImage(original, new Size(width, heigth));
            resized.Save(GetTumbPath(filepath));
        }
    }

    public static string GetTumbPath(string filepath)
    {
        string fileName = Path.GetFileNameWithoutExtension(filepath);
        string ext = Path.GetExtension(filepath);
        return Path.Combine(Path.GetDirectoryName(filepath), fileName + "_s" + ext);
    }

    public static string GetCoverPath(string filepath)
    {
        string fileName = Path.GetFileNameWithoutExtension(filepath);
        string ext = Path.GetExtension(filepath);
        return Path.Combine(Path.GetDirectoryName(filepath), fileName + "_t" + ext);
    }

    public static string GetBigCoverPath(string filepath)
    {
        string fileName = Path.GetFileNameWithoutExtension(filepath);
        string ext = Path.GetExtension(filepath);
        return Path.Combine(Path.GetDirectoryName(filepath), fileName + "_m" + ext);
    }

    public static string GetCoverPathDisplay(string filepath)
    {
        string fileName = Path.GetFileNameWithoutExtension(filepath);
        string ext = Path.GetExtension(filepath);
        return Path.GetDirectoryName(filepath) + "/" + fileName + "_t" + ext;
    }

    public static string GetTumbPathDisplay(string filepath)
    {
        string fileName = Path.GetFileNameWithoutExtension(filepath);
        string ext = Path.GetExtension(filepath);
        return Path.Combine(Path.GetDirectoryName(filepath), fileName + "_s" + ext).Replace("\\", "/").Replace(":/", "://");
    }

    public static void SaveBigThumb(string filepath, int width, int heigth)
    {
        if (!File.Exists(GetBigCoverPath(filepath)))
        {
            Image original = Image.FromFile(filepath);
            Image resized = ResizeImage(original, new Size(width, heigth));
            resized.Save(GetBigCoverPath(filepath));
        }
    }

    public static Unit GetWidthOfImage(string fileName)
    {
        string filepath = UrlHelper.StaticUrlPath("CampaignsUpload/" + Path.GetFileName(fileName));
        System.Drawing.Image img = System.Drawing.Image.FromFile(filepath);
        return img.Width;
    }

    public static void SaveProfilesImages(int userid, string filename)
    {
        string smallpic = Path.Combine(UrlHelper.StaticUrlPath("Profiles"), "s" + userid + ".jpg");
        string bigpic = Path.Combine(UrlHelper.StaticUrlPath("Profiles"), "m" + userid + ".jpg");
        Image original = Image.FromFile(filename);
        Image resized = ResizeImage(original, new Size(32, 32), false);
        resized.Save(smallpic);

        resized = ResizeImage(original, new Size(150, 190));
        resized.Save(bigpic);
    }
}