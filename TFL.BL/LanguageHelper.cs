﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for LanguageHelper
/// </summary>
public class LanguageHelper
{
    private static string CultureCookie = "CI";
    private static string CountryCookie = "CO";
    public static void SetDefaultLanguage()
    {
        string lang = GetLanguage();
        System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(lang);
        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(lang);
        //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lang);
        //Thread.CurrentThread.CurrentUICulture = new CultureInfo(lang);
    }

    public static void SetLanguage(string lang)
    {
        UserData user = UserData.GetUserDataCookie();
        if (user == null)
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(lang);
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(lang);
            HttpContext.Current.Response.Cookies.Add(new HttpCookie(CultureCookie, lang) { Path = "/" });
        }
        else
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(lang);
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(lang);
            SqlUser.ChangeCulture(lang);
        }
    }

    public static string GetLanguage()
    {
        try
        {
            UserData user = UserData.GetUserDataCookie();
            if (user != null)
            {
                return user.CultureName;
            }
            else
            {
                HttpCookie culture = HttpContext.Current.Request.Cookies[CultureCookie];
                if (culture != null && culture.Value != null)
                {
                    return culture.Value;
                }
            }
        }
        catch (Exception)
        {
            return "en";
        }
        string lang = "en";
        if (IfIsrael())
            lang = "he";
        HttpContext.Current.Response.Cookies.Add(new HttpCookie(CultureCookie, lang) { Path = "/" });

        return lang;
    }

    public static bool IfIsrael()
    {
        string countryName = GetCountry();
        return countryName == "il";
    }

    public static string GetStyleByLanguage()
    {
        return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.IsRightToLeft
            ? UrlHelper.StaticUrl(ConfigurationManager.AppSettings["rtlstyle"])
            : UrlHelper.StaticUrl(ConfigurationManager.AppSettings["ltrstyle"]);
    }

    public static string GetCountry(string unknownStr = "")
    {
        HttpCookie countrycookie = HttpContext.Current.Request.Cookies[CountryCookie];
        if (countrycookie != null && countrycookie.Value != null)
            return countrycookie.Value;
        string country = unknownStr;
        try
        {
            HttpWebRequest hrqURL = (HttpWebRequest)HttpWebRequest.Create("http://www.whatismyip.com/");
            Random r = new Random();
            hrqURL.Method = "GET";
            hrqURL.ContentType = "application/x-www-form-urlencoded";
            hrqURL.UserAgent = IPClass.GetIPAddress();
            hrqURL.Headers.Add("X-Forwarded-For", IPClass.GetIPAddress());
            hrqURL.Headers.Add("Accept-Language", "en;q=0.8");
            hrqURL.Proxy = (WebProxy)GlobalProxySelection.Select;
            hrqURL.Proxy.Credentials = CredentialCache.DefaultCredentials;
            HttpWebResponse hrspURL = (HttpWebResponse)hrqURL.GetResponse();
            StreamReader srdrInput
                = new StreamReader
            (hrspURL.GetResponseStream(), Encoding.UTF8);

            string html = srdrInput.ReadToEnd();
            string result = ((System.Text.RegularExpressions.Capture)(Regex.Matches(html, "<div class=\"the-country\">(.*?) -")[0].Groups[1])).Value.ToLower();

            if (result != "")
                country = result;
        }
        catch (Exception ex)
        {
        }
        HttpContext.Current.Response.Cookies.Add(new HttpCookie(CountryCookie, country) { Path = "/" });
        //ExceptionUtility.NotifySystemOps(new ExceptionUtility()
        //{
        //    Error = country,
        //    ErrorBy = IPClass.GetIPAddress()
        //});
        return country;
    }
}