﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UrlHelper
/// </summary>
namespace TFL_DAL
{
    public class UrlHelper
    {
        //for static subdomin

        public static string StaticUrl(string url)
        {
            //return sub domain "http://static.topfashionlist.com"
            return ConfigurationManager.AppSettings["TopFashionListStaticUrl"] + url;
        }

        public static string StaticUrlPath(string url)
        {
            //return HttpContext.Current.Server.MapPath(url);
            return HttpContext.Current.Server.MapPath(url).Replace("topfashionlist.com", "static.topfashionlist.com");
        }

        public static string CampaignUploads(string url)
        {
            return StaticUrl("CampaignsUpload/" + url);
        }

        public static string GetProfileSmall(string userid)
        {
            string url = StaticUrlPath("/Profiles/s" + userid + ".jpg");
            return (File.Exists(url) ? StaticUrl("/Profiles/s" + userid + ".jpg") : StaticUrl("/Profiles/sNone.jpg"));
        }

        public static string GetProfileMedium(string userid)
        {
            string url = StaticUrlPath("/Profiles/m" + userid + ".jpg");
            return (File.Exists(url) ? StaticUrl("/Profiles/m" + userid + ".jpg") : StaticUrl("/Profiles/mNone.jpg"));
        }

        public static string ArticleUploads(string url)
        {
            return StaticUrl("ArticleUpload/" + url);
        }

        public static string TempUploads(string url)
        {
            return StaticUrl("TempFiles/" + url);
        }

    }
}