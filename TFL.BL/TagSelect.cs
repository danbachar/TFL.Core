﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for TagSelect
/// </summary>
public class TagSelect
{
    public string id { get; set; }
    public string name { get; set; }
    public static List<TagSelect> GeTagByName(string key)
    {
        List<TagSelect> list = new List<TagSelect>();
        string culture = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
        SqlParameter[] parameters = { new SqlParameter("@key", SqlDbType.VarChar) { Value = key }, new SqlParameter("@culture", SqlDbType.VarChar) { Value = culture } };
        DataTable cDataTable =
            DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString,
                parameters, "ResourcesByTagName");
        foreach (DataRow tag in cDataTable.Rows)
        {
            list.Add(new TagSelect()
            {
                id = tag["resourceKey"].ToString(),
                name = tag["resourceValue"].ToString()
            });
        }
        return list;
    }

    public static List<TagSelect> GetAllTags()
    {
        return ResourceHelper.GetAllTags();
    }

    public static int AddTag(string tagname)
    {
        List<TagSelect> list= ResourceHelper.GetAllTags();

        int tagmax = 0;
        try
        {
            tagmax = list.Max(x => Convert.ToInt32(x.id.Substring(1))) + 1;
        }
        catch (Exception)
        {

            tagmax = 1;
        }


        SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
        SqlCommand sqlCommand = new SqlCommand("CreateTag", sqlConnection);

        sqlCommand.CommandType = CommandType.StoredProcedure;
        sqlCommand.Parameters.Add("@tagid", SqlDbType.VarChar).Value = "t" + tagmax;
        sqlCommand.Parameters.Add("@tagname", SqlDbType.VarChar).Value = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(tagname.ToLower());
        sqlCommand.Parameters.Add("@culture", SqlDbType.VarChar).Value = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;

        try
        {
            sqlConnection.Open();
            sqlCommand.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            throw e;
        }
        finally
        {
            sqlConnection.Close();
        }
        ResourceHelper.ClearCache();
        return tagmax;
    }

    public static void DeleteSelected(string[] id)
    {
        foreach (string tagSelect in id)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("DeleteTag", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@tagid", SqlDbType.VarChar).Value = tagSelect;
            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }
    }

}