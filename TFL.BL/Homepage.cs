﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for Homepage
/// </summary>
public class Homepage
{
    public List<Campaign> campaigns { get; set; }
    public List<Article> news { get; set; }	

    public static string GetHomepageLists()
    {
        Homepage h = new Homepage()
        {
            campaigns = Campaign.GetLastCampaigns(),
            news=Article.GetLastArticles()
        };
        return new JavaScriptSerializer().Serialize(h);
    }

    public static string ArticleShortDescription(string description,int length)
    {
        string desc = Regex.Replace(description, "<.*?>", string.Empty);
        if (desc.Length > length)
            desc = desc.Substring(0, length) + "...";
        return desc;
    }
}