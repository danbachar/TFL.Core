﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.ModelBinding;
using Newtonsoft.Json;
using System.Web.Security;
using TFL.DAL;

/// <summary>
/// Summary description for UserData
/// </summary>
namespace TFL.BL
{
    public class UserData
    {
        public enum UserRole
        {
            Admin,
            Editor,
            Guest
        }

        public static UserData LoginUser(string email, string password, bool isPersistent)
        {
            UserData login = SqlUser.Login(email, password);
            if (login != null && login.LoginStatus == SqlUser.LoginStatus.Success)
            {
                string cookieName = ConfigurationManager.AppSettings["CookieName"];
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, email, DateTime.Now, DateTime.Now.AddYears(1), isPersistent, new JavaScriptSerializer().Serialize(login), FormsAuthentication.FormsCookiePath);
                string encTicket = FormsAuthentication.Encrypt(ticket);
                HttpContext.Current.Response.Cookies.Add(new HttpCookie(cookieName, encTicket));
            }
            return login;
        }

        public static UserData GetUserDataCookie()
        {
            string cookieName = ConfigurationManager.AppSettings["CookieName"];
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[cookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                return new JavaScriptSerializer().Deserialize<UserData>(ticket.UserData);
            }
            return null;
        }

        public static void LogoutUser()
        {
            string cookieName = ConfigurationManager.AppSettings["CookieName"];
            HttpCookie myCookie = new HttpCookie(cookieName);
            myCookie.Expires = DateTime.Now.AddDays(-1d);
            HttpContext.Current.Response.Cookies.Add(myCookie);
        }

        public static bool ValidLogin(string email, string password)
        {
            bool isEmail = Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            bool passwordlength = password.Length >= 6;
            return isEmail && passwordlength;
        }

        public static string ValidRegister(string first, string last, int userType, bool gender, string birth, string email, string pass, bool newsletter, bool acceptterms)
        {
            DateTime dt;
            bool birthIsValid = DateTime.TryParse(birth, out dt);
            bool isEmail = Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);

            //return&& () &&  && && ;
            List<string> errors = new List<string>();
            if (first.Length == 0)
            {
                errors.Add(ResourceHelper.GetString("sNotValidFirstName"));
            }
            if (!(userType == 3 || userType == 14 || last.Length > 0))
            {
                errors.Add(ResourceHelper.GetString("sNotValidLastName"));
            }
            if (!isEmail)
            {
                errors.Add(ResourceHelper.GetString("sNotValidEmail"));
            }
            if (!(pass.Length >= 6 && pass.Length <= 20))
            {
                errors.Add(ResourceHelper.GetString("sNotValidPasswordLength"));
            }
            if (!acceptterms)
            {
                errors.Add(ResourceHelper.GetString("sNotAcceptedTerms"));
            }
            if (errors.Count == 0)
                return string.Empty;
            return string.Join("<br>", errors);
        }

        public static SqlUser.RegisterStatus CreateUser(string first, string last, bool gender, string birth, string countryid, string cityid, string email, string pass, bool newsletter)
        {
            string g = Guid.NewGuid().ToString();
            SqlUser.RegisterStatus status = SqlUser.CreateUser(first, last, (int)UserRole.Guest, gender, birth, countryid, cityid, "", "", email, pass, newsletter, System.Threading.Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName, g);
            if (status == SqlUser.RegisterStatus.SuccessRegister)
                EmailQueue.AddEmail(new ApproveUserMail(
                    email,
                    ResourceHelper.GetString("sApprovedUser").ToString(),
                    string.Format(ResourceHelper.GetString("sApproveMail").ToString(),
                        first + " " + last,
                        email,
                        ConfigurationManager.AppSettings["TopFashionListUrl"] + string.Format(ConfigurationManager.AppSettings["ApproveUrl"], g))));
            return status;
        }

        public static SqlUser.RegisterStatus AddUser(string first, string last, bool gender, int typeid, string birth, string countryid, string cityid, string email, string pass, bool newsletter, ModelProperty modelProperty = null, string biography = null, List<Agency> agencies = null, int userid = -1)
        {
            SqlUser.RegisterStatus status = SqlUser.CreateUser(first, last, typeid, gender, birth, countryid, cityid, biography, "", email, pass, newsletter, System.Threading.Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName, "-1", modelProperty, agencies, userid);
            return status;
        }

        public static string ApproveUserMail(string guid)
        {
            return SqlUser.ApproveUserMail(guid);
        }


        public static void ChangePassword(int userID, string password)
        {
            SqlUser.ChangePassword(userID, password);
        }

        public static string SendPassword(string email)
        {
            return SqlUser.SendPassword(email);
        }

        public static string GeneratePassword()
        {
            Random rnd = new Random();
            int length = rnd.Next(7, 15);
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
            var random = new Random();
            return new string(
                Enumerable.Repeat(chars, length)
                    .Select(s => s[random.Next(s.Length)])
                    .ToArray());
        }
    }
}