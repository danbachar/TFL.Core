﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TFL_DAL;

namespace TFL_BL
{
    class UserSearch
    {
        public static List<UserSearch> SearchFreeText(string key)
        {
            return SqlUserSelect.SearchFreeText(key);
        }
    }
}
