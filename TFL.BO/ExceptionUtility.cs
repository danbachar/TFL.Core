﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

/// <summary>
/// Summary description for ExceptionUtility
/// </summary>
namespace TFL.BO
{
    public class ExceptionUtility
    {
        private static string logFile = "~/App_Data/ErrorLog.txt";

        public string ErrorTime { get; set; }
        public long ErrorTicks { get; set; }
        public string ErrorBy { get; set; }
        public string ErrorUrl { get; set; }
        public string Error { get; set; }

        // Log an Exception
        public static void LogException(ExceptionUtility exc)
        {
            // Include enterprise logic for logging exceptions
            // Get the absolute path to the log file
            string logPath = HttpContext.Current.Server.MapPath(logFile);
            StreamWriter sw = new StreamWriter(logPath, true);
            string json = JsonConvert.SerializeObject(exc);
            sw.WriteLine(json);
            sw.Close();
        }

        public static void NotifySystemOps(ExceptionUtility exception)
        {
            EmailQueue.AddEmail(new ErrorMail(exception));
        }

        public static string GetAllLogs()
        {
            string logPath = HttpContext.Current.Server.MapPath(logFile);
            List<ExceptionUtility> list = new List<ExceptionUtility>();
            if (File.Exists(logPath))
            {
                using (StreamReader reader = new StreamReader(logPath))
                {
                    while (!reader.EndOfStream)
                    {
                        list.Add(JsonConvert.DeserializeObject<ExceptionUtility>(reader.ReadLine()));
                    }
                }
            }
            return JsonConvert.SerializeObject(list);
        }

        public static void DeleteError(string errortime, string errorby)
        {
            string logPath = HttpContext.Current.Server.MapPath(logFile);
            List<ExceptionUtility> list = new List<ExceptionUtility>();
            using (StreamReader reader = new StreamReader(logPath))
            {
                while (!reader.EndOfStream)
                {
                    list.Add(JsonConvert.DeserializeObject<ExceptionUtility>(reader.ReadLine()));
                }
            }
            var element = list.FirstOrDefault(x => x.ErrorTime == errortime && x.ErrorBy == errorby);
            list.Remove(element);
            CleanErrorLog();
            foreach (ExceptionUtility exceptionUtility in list)
            {
                LogException(exceptionUtility);
            }
        }

        public static void CleanErrorLog()
        {
            string logPath = HttpContext.Current.Server.MapPath(logFile);
            File.Delete(logPath);
        }
    }
}