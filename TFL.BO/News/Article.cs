﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using TFL_BL.Campaign.Campaign;
using TFL_DAL.DataAccess;

/// <summary>
/// Summary description for Article
/// </summary>
namespace TFL.BO.News
{
    public class Article
    {
        public long ArticleID { get; set; }
        public string ArticleName { get; set; }
        public string ArticleTypeName { get; set; }
        public string ArticleDescription { get; set; }
        public string ArticleCreated { get; set; }
        public string ArticleCover { get; set; }


        public string ClientPath { get; set; }
        public string ClientName { get; set; }
        public int CountOfComments { get; set; }
        public int TypeId { get; set; }

        public static List<Article> GetLastArticles()
        {
            List<Article> list = new List<Article>();
            SqlParameter[] parameters = { new SqlParameter("@NumOfNews", SqlDbType.Int) { Value = 3 } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "GetLastNews");
            foreach (DataRow article in cDataTable.Rows)
            {
                list.Add(new Article()
                {
                    ArticleID = (long)article["ArticleID"],
                    ArticleName = article["ArticleName"].ToString(),
                    ArticleCreated = Convert.ToDateTime(article["articlecreated"].ToString()).ToString("dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture),
                    ArticleTypeName = ResourceHelper.GetString(article["articleTypeName"].ToString()).ToString(),
                    ArticleDescription = Homepage.ArticleShortDescription(article["ArticleDescription"].ToString(), 190),
                    ArticleCover = article["articlecover"].ToString(),
                    ClientPath = MemberPath.GetMemberPath(Convert.ToInt32(article["clientuserid"].ToString()), article["clientfirstname"].ToString(), article["clientlastname"].ToString(), Convert.ToInt32(article["professionTypeId"].ToString())),
                    ClientName = article["clientfirstname"].ToString() + " " + article["clientlastname"].ToString()
                });
            }
            return list;
        }

        public static List<Article> GetArticleByTypeID(int typeid, int lastseen)
        {
            List<Article> list = new List<Article>();
            SqlParameter[] parameters = { new SqlParameter("@typeid", SqlDbType.Int) { Value = typeid }, new SqlParameter("@lastseen", SqlDbType.Int) { Value = lastseen } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "GetNewsByTypeID");
            foreach (DataRow article in cDataTable.Rows)
            {
                list.Add(new Article()
                {
                    ArticleID = (long)article["ArticleID"],
                    ArticleName = article["ArticleName"].ToString(),
                    ArticleCreated = Convert.ToDateTime(article["articlecreated"].ToString()).ToString("dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture),
                    ArticleTypeName = ResourceHelper.GetString(article["articleTypeName"].ToString()).ToString(),
                    ArticleDescription = Homepage.ArticleShortDescription(article["ArticleDescription"].ToString(), 100),
                    ArticleCover = article["articlecover"].ToString(),
                    ClientPath = MemberPath.GetMemberPath(Convert.ToInt32(article["authoruserid"].ToString()), article["authorfirstname"].ToString(), article["authorlastname"].ToString(), Convert.ToInt32(article["professionTypeId"].ToString())),
                    ClientName = article["authorfirstname"].ToString() + " " + article["authorlastname"].ToString()
                });
            }
            return list;
        }

        public static List<Campaign> GetArticleByUserIDCastCampaign(int lastread, int userid)
        {
            List<Campaign> list = new List<Campaign>();
            SqlParameter[] parameters = { new SqlParameter("@userid", SqlDbType.Int) { Value = userid }, new SqlParameter("@lastread", SqlDbType.Int) { Value = lastread } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "GetNewsByUserID");
            foreach (DataRow camp in cDataTable.Rows)
            {
                list.Add(new Campaign()
                {
                    CampaignID = (long)camp["articleID"],
                    CampaignName = camp["articleName"].ToString(),
                    CampaignPublished = Convert.ToDateTime(camp["articlecreated"].ToString()),
                    CampaignDatePublishText = Convert.ToDateTime(camp["articlecreated"].ToString()).ToString("dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentCulture),
                    CampaignTypeName = ResourceHelper.GetString(camp["articleTypeName"].ToString()).ToString(),
                    CampaignDescription = Homepage.ArticleShortDescription(camp["ArticleDescription"].ToString(), 90),
                    CampaignAlbumCover = camp["articlecover"].ToString(),
                    ClientName = camp["firstname"].ToString() + " " + camp["lastname"].ToString(),
                    ClientPath = MemberPath.GetMemberPath(Convert.ToInt32(camp["userid"].ToString()), camp["firstname"].ToString(), camp["lastname"].ToString(), Convert.ToInt32(camp["professionTypeId"].ToString()))
                });
            }
            return list;
        }

        public static List<Campaign> GetModelsByUserIDCastCampaign(int lastread, int userid)
        {
            List<Campaign> list = new List<Campaign>();
            SqlParameter[] parameters = { new SqlParameter("@agencyid", SqlDbType.Int) { Value = userid }, new SqlParameter("@lastread", SqlDbType.Int) { Value = lastread }, new SqlParameter("@culture", SqlDbType.VarChar) { Value = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName } };
            DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "GetModelsByAgency");
            foreach (DataRow camp in cDataTable.Rows)
            {
                list.Add(new Campaign()
                {
                    CampaignID = Convert.ToInt64(camp["userid"].ToString()),
                    CampaignName = camp["agencyname"].ToString(),
                    CampaignAlbumCover = UrlHelper.GetProfileMedium(camp["userid"].ToString())
                });
            }
            return list;
        }

        public static void ApproveArticle(int articleid)
        {
            SqlArticle.ChangeArticleStatus(articleid, 1);
        }

        public static void RejectArticle(int articleid)
        {
            SqlArticle.ChangeArticleStatus(articleid, -1);
        }

        public static List<Article> SearchArticles(Article article)
        {
            return SqlArticle.SearchArticles(article);
        }

        public static void DeleteArticle(int id)
        {
            SqlArticle.DeleteArticle(id);
        }
    }
}