﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for ArticleCategory
/// </summary>
namespace TFL.BO.News.ArticleCategory
{
    public class ArticleCategory
    {
        public string id { get; set; }
        public string name { get; set; }

        public static List<ArticleCategory> GetAllCategories()
        {
            return ResourceHelper.GetAllArticlesCategories();
        }

        public static void AddCategory(string text)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("CreateArticleCategory", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@categoryname", SqlDbType.VarChar).Value = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(text.ToLower());
            sqlCommand.Parameters.Add("@culture", SqlDbType.VarChar).Value = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;

            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
            ResourceHelper.ClearCache();
        }

        public static void DeleteSelected(string[] id)
        {
            foreach (string category in id)
            {
                SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
                SqlCommand sqlCommand = new SqlCommand("DeleteArticleCategory", sqlConnection);

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add("@categoryid", SqlDbType.VarChar).Value = category;
                try
                {
                    sqlConnection.Open();
                    sqlCommand.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    throw e;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
        }
    }
}