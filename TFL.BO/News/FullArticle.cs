﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for FullArticle
/// </summary>
namespace TFL.BO.News
{
    public class FullArticle : Article
    {

        public string Creator { get; set; }
        public int ArticleTypeID { get; set; }
        public string Users { get; set; }
        public string Tags { get; set; }

        public bool IsApproved { get; set; }
        //For Edit
        public List<UserSelect> UsersList { get; set; }
        public List<TagSelect> TagsList { get; set; }
        public string AuthorName { get; set; }
        public int AuthorID { get; set; }
        public string ClientPath { get; set; }
        public int AuthorType { get; set; }
        public DateTime ArticleCreated { get; set; }

        public int SaveArticle()
        {
            return SqlArticle.SaveArticle(this);
        }

        public static FullArticle GetArticleById(int articleid)
        {
            return SqlArticle.GetArticleById(articleid);
        }
    }
}