﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFL.BO
{
    public class UserSelect
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Label{ get; set; }
    }
}
