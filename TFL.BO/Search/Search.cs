﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Search
/// </summary>
public class Search
{
    public int TypeID { get; set; }
    public int ProfessionID { get; set; }
    public string Str { get; set; }
    public int Gender { get; set; }
    public int FromAge { get; set; }
    public int ToAge { get; set; }
    public string Country { get; set; }
    public string State { get; set; }
    public string City { get; set; }
    public string Tags { get; set; }

    public int SearchID { get; set; }
    public string PathUrl { get; set; }
    public string ImagePath { get; set; }

    public int PageNumber { get; set; }

    public List<Search> GetList()
    {
        const int RowsAPage = 20;
        List<Search> list = new List<Search>();
        SqlParameter[] parameters =
        {
            new SqlParameter("@searchType", SqlDbType.Int) { Value = this.TypeID },
            new SqlParameter("@professionid", SqlDbType.Int) { Value = this.ProfessionID },
            new SqlParameter("@key", SqlDbType.VarChar) { Value = this.Str },
            new SqlParameter("@gender", SqlDbType.Int) { Value = this.Gender },
            new SqlParameter("@fromage", SqlDbType.Int) { Value = this.FromAge },
            new SqlParameter("@toage", SqlDbType.Int) { Value = this.ToAge },
            new SqlParameter("@countryid", SqlDbType.VarChar) { Value = (this.Country ?? "") },
            new SqlParameter("@cityid", SqlDbType.VarChar) { Value = (this.City ?? "") },
            new SqlParameter("@tags", SqlDbType.VarChar) { Value = this.Tags.Replace("t","") },
            new SqlParameter("@RowspPage", SqlDbType.Int) { Value = RowsAPage },
            new SqlParameter("@PageNumber", SqlDbType.Int) { Value = PageNumber }

            
        };
        DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "SearchAll");
        foreach (DataRow row in cDataTable.Rows)
        {
            list.Add(new Search()
            {
                SearchID = Convert.ToInt32(row["id"].ToString()),
                Str = row["fullname"].ToString(),
                PathUrl = (this.TypeID == 1 ? MemberPath.GetMemberPath(Convert.ToInt32(row["id"].ToString()), row["firstname"].ToString(), row["lastname"].ToString(), Convert.ToInt32(row["professionTypeId"].ToString())) : this.TypeID == 2 ? "Campaign/" + row["id"] : "Article/" + row["id"]),
                ImagePath = GetImage(this.TypeID, Convert.ToInt32(row["id"].ToString()), row["cover"].ToString())
            });
        }
        return list;
    }

    private static string GetImage(int rowtype, int userid, string text)
    {
        switch (rowtype)
        {
            case 1:
                return UrlHelper.GetProfileMedium(userid.ToString());
            case 2:
                return UrlHelper.CampaignUploads(text);
            case 3:
                return text;
        }
        return "";
    }
}