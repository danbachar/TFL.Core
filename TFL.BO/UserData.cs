﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFL.BO
{
    public class UserData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public UserRole UserType { get; set; }
        public string CultureName { get; set; }
        public SqlUser.LoginStatus LoginStatus { get; set; }
        public int UserID { get; set; }
        public string UserPath { get; set; }
        public string CountryID { get; set; }
        public string CityID { get; set; }
    }
}