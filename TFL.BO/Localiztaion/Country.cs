﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Country
/// </summary>
public class Country
{
    public string CountryCode { get; set; }
    public string CountryName { get; set; }
}