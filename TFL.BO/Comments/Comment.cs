﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Comment
/// </summary>
public class Comment
{
    public int CommentId { get; set; }
    public string ParentId { get; set; }
    public string CommentString { get; set; }
    public int UserId { get; set; }
    public string UserName { get; set; }
    public DateTime CommentTime { get; set; }
    public string ProfilePhoto { get; set; }
    public bool CanDelete { get; set; }

    public static int AddCommentToParent(string parent, string comment)
    {
        if (parent.Split('/').Length > 3)
            parent = "/" + parent.Split('/')[1] + "/" + parent.Split('/')[2];
        return SqlComment.AddCommentToParent(parent, comment);
    }
}