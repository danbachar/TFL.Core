﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

/// <summary>
/// Summary description for FullComment
/// </summary>
public class FullComment
{
    public bool IsLogin { get; set; }
    public string ProfilePath { get; set; }
    public string UserName { get; set; }
    public List<Comment> Comments { get; set; }

    public static FullComment GetCommentByParent(string parent)
    {
        if (parent.Split('/').Length > 3)
            parent = "/" + parent.Split('/')[1] + "/" + parent.Split('/')[2];
        return SqlComment.GetCommentByParent(parent);
    }
}