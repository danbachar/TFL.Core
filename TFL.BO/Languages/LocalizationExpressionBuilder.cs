﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Compilation;

/// <summary>
/// Summary description for LocalizationExpressionBuilder
/// </summary>
public class LocalizationExpressionBuilder : ExpressionBuilder
{
    public override CodeExpression GetCodeExpression(System.Web.UI.BoundPropertyEntry entry, object parsedData, ExpressionBuilderContext context)
    {
        CodeExpression[] inputParams = new CodeExpression[] { new CodePrimitiveExpression(entry.Expression.Trim()), 
                                                    new CodeTypeOfExpression(entry.DeclaringType), 
                                                    new CodePrimitiveExpression(entry.PropertyInfo.Name) };

        // Return a CodeMethodInvokeExpression that will invoke the GetRequestedValue method using the specified input parameters 
        return new CodeMethodInvokeExpression(new CodeTypeReferenceExpression(this.GetType()),
                                    "GetRequestedValue",
                                    inputParams);
    }

    public static object GetRequestedValue(string key, Type targetType, string propertyName)
    {
        // If we reach here, no type mismatch - return the value 
        //return GetByText(key);
        string ResourceType = key.Split(',')[0].Trim();
        key = key.Split(',')[1].Trim();
        switch (ResourceType)
        {
            case "Resource":
                return ResourceHelper.GetString(key);                
            case "ModelPropertiesResource":
                return ResourceHelper.GetModelPropertiesString(key);
            case "BigText":
                return ResourceHelper.GetBigTextString(key);
        }
        return key;
    }

    //Place holder until database is build
    public static string GetByText(string text)
    {
        return text;
    }
}