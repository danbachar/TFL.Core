﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LanguagePair
/// </summary>
public class LanguagePair
{
    public string FromValue { get; set; }
    public string ToValue { get; set; }
}