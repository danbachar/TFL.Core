﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LanguageCulture
/// </summary>
public class LanguageCulture
{
    public string CultureID { get; set; }
    public string CultureName { get; set; }
    public string Countryid { get; set; }
}