﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Ionic.Zip;

/// <summary>
/// Summary description for GeneralSettings
/// </summary>
public class GeneralSettings
{
    private const string SettingsName = "Settings";

    public static Dictionary<string, string> GetDictionarySettings()
    {
        Dictionary<string, string> dictionary = new Dictionary<string, string>();
        try
        {
            if (HttpContext.Current.Cache[SettingsName] == null ||
                ((Dictionary<string, string>)HttpContext.Current.Cache[SettingsName]).Count == 0)
            {
                SqlParameter[] parameters = { };
                DataTable cDataTable =
                    DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString,
                        parameters, "GetGeneralSettings");
                foreach (DataRow keyVal in cDataTable.Rows)
                {
                    if (!dictionary.ContainsKey(keyVal["SettingKey"].ToString()))
                        dictionary.Add(keyVal["SettingKey"].ToString(), keyVal["SettingValue"].ToString());
                }
                HttpContext.Current.Cache[SettingsName] = dictionary;
            }
            else
            {
                dictionary = (Dictionary<string, string>)HttpContext.Current.Cache[SettingsName];
            }
        }
        catch (Exception)
        {
            ClearCache();
            return GetDictionarySettings();
        }
        return dictionary;
    }

    public static void ClearCache()
    {
        //HttpContext.Current.Cache.Remove(SettingsName);
        foreach (System.Collections.DictionaryEntry entry in HttpContext.Current.Cache)
        {
            HttpContext.Current.Cache.Remove(entry.Key.ToString());
        }
    }

    public static string GetSettingValue(string key)
    {
        Dictionary<string, string> dictionary = GetDictionarySettings();
        return dictionary.ContainsKey(key) ? dictionary[key] : "";
    }

    public static void SaveSettings(DataTable dictionary)
    {
        SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
        SqlCommand sqlCommand = new SqlCommand("SaveGeneralSettings", sqlConnection);

        sqlCommand.CommandType = CommandType.StoredProcedure;

        sqlCommand.Parameters.Add("@Languages", SqlDbType.Structured).Value = dictionary;
        try
        {
            sqlConnection.Open();
            sqlCommand.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            throw e;
        }
        finally
        {
            sqlConnection.Close();
        }
        ClearCache();
    }

    public static void RemoveImages(bool withDownload)
    {
        SqlParameter[] parameters = { };
        DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "GetAllImages");
        List<string> usedImages = new List<string>();
        foreach (DataRow imageOrHtml in cDataTable.Rows)
        {
            if (imageOrHtml["htmlorsource"].ToString().ToLower().IndexOf("<img") > -1)
            {
                foreach (Match m in Regex.Matches(imageOrHtml["htmlorsource"].ToString().ToLower(), "<img.+?src=[\"'](.+?)[\"'].+?>", RegexOptions.IgnoreCase | RegexOptions.Multiline))
                {
                    usedImages.Add(System.IO.Path.GetFileName(m.Groups[1].Value));
                }
            }
            else
            {
                usedImages.Add(imageOrHtml["htmlorsource"].ToString().ToLower());
            }
        }
        List<string> AllImages = Directory.GetFiles(UrlHelper.StaticUrlPath("~/ArticleUpload")).ToList();
        AllImages.AddRange(Directory.GetFiles(UrlHelper.StaticUrlPath("~/CampaignsUpload")).ToList());
        List<string> removeimages = new List<string>();
        foreach (string allImage in AllImages)
        {
            bool exist = false;
            string ImageInDirectory = allImage.Replace("_s", string.Empty).Replace("_t", string.Empty);
            foreach (string usedImage in usedImages)
            {
                string imageInCampaignOrArticle = UrlHelper.StaticUrlPath("~/ArticleUpload/" + usedImage.Replace("_s", string.Empty).Replace("_t", string.Empty));
                if (ImageInDirectory == imageInCampaignOrArticle)
                {
                    exist = true;
                    break;
                }
                imageInCampaignOrArticle = UrlHelper.StaticUrlPath("~/CampaignsUpload/" + usedImage.Replace("_s", string.Empty).Replace("_t", string.Empty));
                if (ImageInDirectory == imageInCampaignOrArticle)
                {
                    exist = true;
                    break;
                }
            }
            if (!exist)
                removeimages.Add(ImageInDirectory);
        }
        if (withDownload)
        {
            try
            {
                string filename = UrlHelper.StaticUrlPath("~/TempFiles/myphotos.zip");
                using (ZipFile zip = new ZipFile())
                {
                    foreach (string removeimage in removeimages.Distinct())
                    {
                        zip.AddFile(removeimage, "images");
                    }
                    zip.Save(filename);
                }

                FileInfo file = new FileInfo(filename);
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
                HttpContext.Current.Response.ContentType = "text/plain";
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.TransmitFile(file.FullName);
                HttpContext.Current.Response.End();
            }
            catch (System.Exception ex1)
            {
                System.Console.Error.WriteLine("exception: " + ex1);
            }

        }
        foreach (string removeimage in removeimages)
        {
            if (File.Exists(removeimage))
                File.Delete(removeimage);
            if (File.Exists(removeimage.Replace(Path.GetExtension(removeimage), "_t" + Path.GetExtension(removeimage))))
                File.Delete(removeimage.Replace(Path.GetExtension(removeimage), "_t" + Path.GetExtension(removeimage)));
            if (File.Exists(removeimage.Replace(Path.GetExtension(removeimage), "_s" + Path.GetExtension(removeimage))))
                File.Delete(removeimage.Replace(Path.GetExtension(removeimage), "_s" + Path.GetExtension(removeimage)));
        }
    }

    public class CompareImages : IEqualityComparer<string>
    {
        public bool Equals(string x, string y)
        {
            try
            {
                string url1 = x.Replace("_s", string.Empty).Replace("_t", string.Empty);
                string url2 = y.Replace("_s", string.Empty).Replace("_t", string.Empty);

                string pathfromdirectory = "", img = "";
                if ((Path.GetDirectoryName(url1) != "" && Path.GetDirectoryName(url2) != "") || (Path.GetDirectoryName(url1) == "" && Path.GetDirectoryName(url2) == ""))
                {
                    return true;
                }
                if (Path.GetDirectoryName(url1) != "")
                {
                    pathfromdirectory = url1;
                    img = url2;
                }
                else if (Path.GetDirectoryName(url2) != "")
                {
                    pathfromdirectory = url2;
                    img = url1;
                }
                string finddirectory = UrlHelper.StaticUrlPath("~/ArticleUpload/" + img);
                if (pathfromdirectory == finddirectory)
                    return true;
                finddirectory = UrlHelper.StaticUrlPath("~/CampaignsUpload/" + img);
                if (pathfromdirectory == finddirectory)
                    return true;
            }
            catch (Exception)
            {
                return false;
            }
            //string path1 = UrlHelper.StaticUrlPath("~/ArticleUpload/" + url1);
            //string path2 = UrlHelper.StaticUrlPath("~/ArticleUpload/" + url2);
            //if (path1 == path2)
            //    return true;
            //path1 = UrlHelper.StaticUrlPath("~/CampaignsUpload/" + url1);
            //path2 = UrlHelper.StaticUrlPath("~/CampaignsUpload/" + url2);
            //if (path1 == path2)
            //    return true;
            return false;
        }

        public int GetHashCode(string obj)
        {
            return 1;
        }
    }
}