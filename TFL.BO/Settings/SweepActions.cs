﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SwipActions
/// </summary>
public class SweepActions
{
    private static int Hours = 0;
    private static int Mintues = 0;

    public static void SweepUnApprovedUsers()
    {
        if (DateTime.Now.Hour == Hours && DateTime.Now.Minute == Mintues)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
            SqlCommand sqlCommand = new SqlCommand("SweepUnapprovedUsers", sqlConnection);

            sqlCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                sqlConnection.Close();
            }
        }
    }
}