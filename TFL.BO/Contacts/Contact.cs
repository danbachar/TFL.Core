﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for Contact
/// </summary>
public class ContactClass
{
    public int CompanyID { get; set; }
    public string CompanyName { get; set; }
    public string ContactName { get; set; }
    public int WorkType { get; set; }
    public string WorkTypeName { get; set; }
    public string CityID { get; set; }
    public string CountryID { get; set; }
    public string Address { get; set; }
    public string Email { get; set; }
    public string Website { get; set; }
    public string Phone { get; set; }

    public static List<ContactClass> SearchContacts(ContactClass contactClass)
    {
        return SqlContact.SearchContacts(contactClass);
    }

    public static List<ContactClass> GetContactsByIDs(string contactsid)
    {
        return SqlContact.GetContactsByIDs(contactsid);
    }
    public static bool ValidRegister(ContactClass contactClass)
    {
        bool isEmail = Regex.IsMatch(contactClass.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);

        return contactClass.CompanyName.Length > 0 && contactClass.ContactName.Length > 0 && isEmail && contactClass.WorkType > 0;

    }

    public static void DeleteContact(int companyid)
    {
        SqlContact.DeleteContact(companyid);
    }
}