﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for EmailQueue
/// </summary>
public class EmailQueue
{
    private static readonly Queue<MailInterface> Emails = new Queue<MailInterface>();
    //private static readonly List<MailLog> Failures = new List<MailLog>();
    //private static readonly List<MailLog> Suecces = new List<MailLog>();
    public static List<string> List = new List<string>();
    public static void AddEmail(MailInterface mail)
    {
        //Monitor.Enter(Emails);
        Emails.Enqueue(mail);
        //Monitor.Exit(Emails);
    }

    public static void SendQueue()
    {
        if (Emails.Count > 0)
        {
            Monitor.Enter(Emails);
            MailInterface mail = Emails.Dequeue();
            if (mail != null)
            {
                MailMessage mailObj = mail.GetMessage();
                try
                {
                    SmtpClient smtp = new SmtpClient();
                    smtp.SendCompleted += (s, e) =>
                    {
                        smtp.Dispose();
                        mailObj.Dispose();
                        //Suecces.Add(new MailLog()
                        //{
                        //    To = mailObj.To.Select(x => x.Address).ToList(),
                        //    CreatedTime = DateTime.Now
                        //});
                    };
                    smtp.SendAsync(mailObj, "");
                }
                catch (Exception exception)
                {
                    //Failures.Add(new MailLog()
                    //{
                    //    To = mailObj.To.Select(x => x.Address).ToList(),
                    //    CreatedTime = DateTime.Now,
                    //    FailureException = exception
                    //});
                }
            }
            Monitor.Exit(Emails);
        }
    }

    //public static List<MailLog> GetSuccessList()
    //{
    //    return Suecces;
    //}
    //public static List<MailLog> GetFailuresList()
    //{
    //    return Failures;
    //}

    //public static void SendSuccessList()
    //{
    //    if (Suecces.Count > 0)
    //    {
    //        Monitor.Enter(Suecces);
    //        MailInterface mail = new SuccessMail(Suecces);
    //        MailMessage mailObj = mail.GetMessage();
    //        try
    //        {
    //            SmtpClient smtp = new SmtpClient();
    //            smtp.Send(mailObj);
    //        }
    //        catch (Exception exception)
    //        {

    //        }
    //        Suecces.Clear();
    //        Monitor.Exit(Suecces);
    //    }
    //}
}