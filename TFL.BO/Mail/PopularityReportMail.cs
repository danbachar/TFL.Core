﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for PopularityReportMail
/// </summary>
public class PopularityReportMail : MailInterface
{
    public PopularityReportMail(string tousers, string pdfpath)
    {
        To = tousers.Split(';').ToList();
        MailFrom = new MailAddress(GeneralSettings.GetSettingValue("MailSentFrom"), GeneralSettings.GetSettingValue("MailSentFromDisplay"));
        Subject = ResourceHelper.GetString("sPopularityReport").ToString() + " - " + ConfigurationManager.AppSettings["WebName"];
        Description = "";
        PdfAttachment = new Attachment(pdfpath);
    }

    public List<string> To { get; set; }
    public MailAddress MailFrom { get; set; }
    public string Subject { get; set; }
    public string Description { get; set; }
    public Attachment PdfAttachment { get; set; }
    public MailMessage GetMessage()
    {
        MailMessage mailObj = new MailMessage()
        {
            From = MailFrom,
            IsBodyHtml = true,
            Subject = Subject,
            Body = Description
        };
        foreach (string s in To)
            mailObj.To.Add(new MailAddress(s));

        mailObj.Attachments.Add(PdfAttachment);
        return mailObj;
    }

}