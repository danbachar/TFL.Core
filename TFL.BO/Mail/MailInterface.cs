﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for MailInterface
/// </summary>
public interface MailInterface
{
    List<string> To { get; set; }
    MailAddress MailFrom { get; set; }
    string Subject { get; set; }
    string Description { get; set; }

    MailMessage GetMessage();
}