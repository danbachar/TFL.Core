﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;

/// <summary>
/// Summary description for KeepAndSendMail
/// </summary>
public class KeepAndSendMail : MailInterface
{
    public List<string> To { get; set; }
    public MailAddress MailFrom { get; set; }
    public string Subject { get; set; }
    public string Description { get; set; }
    public AlternateView ItemAlternateView { get; set; }

    public KeepAndSendMail(string to, List<KeepAndSendItem> items)
    {
        To = new List<string>() { to };
        MailFrom = new MailAddress(GeneralSettings.GetSettingValue("MailSentFrom"), GeneralSettings.GetSettingValue("MailSentFromDisplay"));
        Subject = ConfigurationManager.AppSettings["WebName"] + " - " + ResourceHelper.GetString("sKeepAndSend").ToString();
        Description = "<h1>" + ConfigurationManager.AppSettings["WebName"] + " - " + ResourceHelper.GetString("sKeepAndSend").ToString() + "</h1><br>" + string.Join("<br>", items.Select(x => "<h3>" + x.ItemTitle + "</h3><a href='" + ConfigurationManager.AppSettings["TopFashionListUrl"] + x.Path + "'><img src=\"cid:" + x.Path + "\"/></a><br>" + x.ItemDescription));
        ItemAlternateView = AlternateView.CreateAlternateViewFromString(Description, null, "text/html");
        foreach (KeepAndSendItem item in items)
        {
            try
            {
                LinkedResource logo1 =
                    new LinkedResource(
                        UrlHelper.StaticUrlPath((item.Path.ToLower().StartsWith("camp")
                            ? "CampaignsUpload\\" + item.ItemCover
                            : "ArticleUpload\\" + Path.GetFileName(item.ItemCover))), MediaTypeNames.Image.Jpeg);
                logo1.ContentId = item.Path;
                ItemAlternateView.LinkedResources.Add(logo1);
            }
            catch
            {
            }
        }

    }

    public MailMessage GetMessage()
    {

        MailMessage mailObj = new MailMessage()
        {
            From = MailFrom,
            IsBodyHtml = true,
            Subject = Subject,
            Body = Description,

        };
        mailObj.AlternateViews.Add(ItemAlternateView);
        foreach (string s in To)
            mailObj.To.Add(new MailAddress(s));
        //here can attach files if needed
        return mailObj;
    }
}