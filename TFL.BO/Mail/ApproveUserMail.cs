﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for ApproveUserMail
/// </summary>
public class ApproveUserMail : MailInterface
{
    public ApproveUserMail(string touser, string sub, string desc)
    {
        To = new List<string>() { touser };
        MailFrom = new MailAddress(GeneralSettings.GetSettingValue("MailSentFrom"), GeneralSettings.GetSettingValue("MailSentFromDisplay"));
        Subject = sub;
        Description = desc;
    }

    public List<string> To { get; set; }
    public MailAddress MailFrom { get; set; }
    public string Subject { get; set; }
    public string Description { get; set; }
    public MailMessage GetMessage()
    {
        MailMessage mailObj = new MailMessage()
        {
            From = MailFrom,
            IsBodyHtml = true,
            Subject = Subject,
            Body = Description
        };
        foreach (string s in To)
            mailObj.To.Add(new MailAddress(s));
        //here can attach files if needed
        return mailObj;
    }
}