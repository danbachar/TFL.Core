﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for ContactMail
/// </summary>
public class ContactMail : MailInterface
{
    public List<string> To { get; set; }
    public MailAddress MailFrom { get; set; }
    public string Subject { get; set; }
    public string Description { get; set; }

    public ContactMail(string fullname, string email, string subject, string body)
    {
        To = new List<string>() { GeneralSettings.GetSettingValue("ContactMailSentTo") };
        MailFrom = new MailAddress(GeneralSettings.GetSettingValue("MailSentFrom"), GeneralSettings.GetSettingValue("MailSentFromDisplay"));
        Subject = ResourceHelper.GetString("sContact").ToString() + " - " + ResourceHelper.GetString("sEmail");
        Description = ResourceHelper.GetString("sFullName") + ": " + fullname + "<br>" +
             ResourceHelper.GetString("sEmail") + ": " + email + "<br>" +
             ResourceHelper.GetString("sSubject") + ": " + subject + "<br>" +
             ResourceHelper.GetString("sDescription") + ": " + body;
    }

    public MailMessage GetMessage()
    {
        MailMessage mailObj = new MailMessage()
        {
            From = MailFrom,
            IsBodyHtml = true,
            Subject = Subject,
            Body = Description
        };
        foreach (string s in To)
            mailObj.To.Add(new MailAddress(s));
        //here can attach files if needed
        return mailObj;
    }
}