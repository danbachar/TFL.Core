﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

/// <summary>
/// Summary description for Campaign
/// </summary>
namespace TFL.BO
{
    public class Campaign
    {
        public long CampaignID { get; set; }
        public string CampaignName { get; set; }
        public DateTime CampaignPublished { get; set; }
        public string CampaignDatePublishText { get; set; }
        public string CampaignTypeName { get; set; }
        public int CampaignTypeID { get; set; }
        public string CampaignDescription { get; set; }
        public string CampaignAlbumCover { get; set; }

        public string ClientName { get; set; }
        public string ClientPath { get; set; }

        public int NudeMode { get; set; }

        public static List<Campaign> GetLastCampaigns()
        {
            return SqlCampaign.GetLastCampaigns();
        }

        public static List<Campaign> GetCampaignByTypeID(int typeid, int lastseen)
        {
            return SqlCampaign.GetCampaignByTypeID(typeid, lastseen);
        }

        public static List<Campaign> GetCampaignByTypeIDAndUserID(int typeid, int lastread, int userid)
        {
            return SqlCampaign.GetCampaignByTypeIDAndUserID(typeid, lastread, userid);
        }

        public static List<Campaign> SearchCampaigns(Campaign campaign)
        {
            return SqlCampaign.SearchCampaigns(campaign);
        }

        public static void DeleteCampaign(int id)
        {
            SqlCampaign.DeleteCampaign(id);
        }

        public static void ApproveCampaign(int id)
        {
            SqlCampaign.ChangeCampaignStatus(id, 1);
        }

        public static void RejectCampaign(int id)
        {
            SqlCampaign.ChangeCampaignStatus(id, -1);
        }
    }
}