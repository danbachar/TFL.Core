﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.Data;
/// <summary>
/// Summary description for FullCampaign
/// </summary>
namespace TFL.BO
{
    public class FullCampaign : Campaign
    {
        //public int CampaignTypeID { get; set; }
        public string Creator { get; set; }

        public string Users { get; set; }
        public string Tags { get; set; }
        public string Videos { get; set; }
        public List<CampaignFiles> Files { get; set; }
        public bool IsPublish { get; set; }
        public string AuthorName { get; set; }
        public int AuthorID { get; set; }
        public int AuthorType { get; set; }
        //For Edit
        public List<UserSelect> UsersList { get; set; }
        public List<TagSelect> TagsList { get; set; }

        public int SaveCampaign()
        {
            return SqlCampaign.SaveCampaign(this);
        }

        public static DataTable GetDataTableFiles(List<CampaignFiles> Files)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("fileid"));
            dt.Columns.Add(new DataColumn("filename"));
            dt.Columns.Add(new DataColumn("description"));
            dt.Columns.Add(new DataColumn("isfirst"));
            dt.Columns.Add(new DataColumn("isnude"));
            dt.Columns.Add(new DataColumn("tags"));
            int index = 1;
            foreach (CampaignFiles file in Files)
            {
                string tags = "";
                if (!string.IsNullOrEmpty(file.Tags))
                {
                    tags = SqlCampaign.AddingNewTags(file.Tags);
                    tags = "t" + tags.Remove(tags.Length - 1).Replace(",", ",t");
                }
                dt.Rows.Add(file.Place, file.FileName, file.Description, file.SetAsFirst, file.IsNude, tags);
            }
            return dt;
        }

        public static FullCampaign GetCampaignByID(int id)
        {
            return SqlCampaign.GetCampaignByID(id);
        }

        public static string GetUsersToDisplay(List<UserSelect> list)
        {
            List<UserSelect> professions = SqlUser.GetAllVisibleUsersProfessions();
            string html = "";
            List<string> links;
            var results = list.GroupBy(p => p.name,
                             (key, g) => new { ProfessionType = key, Users = g.ToList() }).ToList();
            foreach (var type in results)
            {
                html += ResourceHelper.GetString(professions.First(x => x.id == type.ProfessionType).label) + ": ";
                links = type.Users.Select(user => "<a href='\\" + MemberPath.GetMemberPath(Convert.ToInt32(user.id), user.label.Substring(0, user.label.IndexOf(".")), user.label.Substring(user.label.IndexOf(".") + 1), Convert.ToInt32(type.ProfessionType)) + "'>" + MemberPath.UpperName(user.label).Replace('.', ' ') + "</a>").ToList();

                html += string.Join(",", links.ToArray()) + "<br>";
            }
            return html;
        }
    }
}