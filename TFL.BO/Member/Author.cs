﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TFL.BO.News;

/// <summary>
/// Summary description for Author
/// </summary>
public class Author
{
    public int Userid { get; set; }
    public string Profile { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public int CountOfArticles { get; set; }

    public static Author GetAuthor(string userpath)
    {
        return SqlMember.GetAuthor(userpath);
    }

    public static List<Article> GetNewsByAuthor(int userid, int rowspage, int pagenumber)
    {
        return SqlArticle.GetNewsByAuthor(userid, rowspage, pagenumber);
    }
}