﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;

/// <summary>
/// Summary description for FacebookMember
/// </summary>
public class FacebookMember
{
    public FacebookMember()
    {
    }

    public string id { get; set; }
    public agerange age_range { get; set; }
    public string bio { get; set; }
    public string email { get; set; }
    public string first_name { get; set; }
    public string last_name { get; set; }
    public string gender { get; set; }
    public string location { get; set; }
    public string website { get; set; }

    public class agerange
    {
        public int min { get; set; }
    }
    public void Login()
    {
        SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
        SqlCommand sqlCommand = new SqlCommand("LoginWithFacebook", sqlConnection);

        sqlCommand.CommandType = CommandType.StoredProcedure;
        sqlCommand.Parameters.Add("@returnValue", SqlDbType.Int, 0).Direction = ParameterDirection.ReturnValue;
        sqlCommand.Parameters.Add("@facebookid", SqlDbType.VarChar).Value = id;
        sqlCommand.Parameters.Add("@bio", SqlDbType.VarChar).Value = bio;
        sqlCommand.Parameters.Add("@birthday", SqlDbType.Date).Value = new DateTime(DateTime.Today.Year - age_range.min, 1, 1);
        sqlCommand.Parameters.Add("@email", SqlDbType.VarChar).Value = email;
        sqlCommand.Parameters.Add("@first_name", SqlDbType.VarChar).Value = first_name;
        sqlCommand.Parameters.Add("@last_name", SqlDbType.VarChar).Value = last_name;
        sqlCommand.Parameters.Add("@gender", SqlDbType.Bit).Value = (gender == "male");
        sqlCommand.Parameters.Add("@countryid", SqlDbType.VarChar).Value = "";
        sqlCommand.Parameters.Add("@cityid", SqlDbType.VarChar).Value = "";
        sqlCommand.Parameters.Add("@website", SqlDbType.VarChar).Value = website;
        sqlCommand.Parameters.Add("@CultureName", SqlDbType.VarChar).Value = System.Threading.Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;
        sqlCommand.Parameters.Add("@professiontypeid", SqlDbType.Int, 0).Direction = ParameterDirection.Output;
       
        try
        {
            sqlConnection.Open();
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            UserData user = new UserData()
            {
                FirstName = first_name,
                LastName = last_name,
                Email = email,
                UserType = (UserData.UserRole)Convert.ToInt32(sqlCommand.Parameters["@professiontypeid"].Value),
                CultureName = System.Threading.Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName,
                UserID = Convert.ToInt32(sqlCommand.Parameters["@returnValue"].Value),
                UserPath = MemberPath.GetMemberPath(Convert.ToInt32(sqlCommand.Parameters["@returnValue"].Value.ToString()), first_name, last_name, Convert.ToInt32(sqlCommand.Parameters["@professiontypeid"].Value)),
                CountryID = "",
                CityID = "",
            };

            string cookieName = ConfigurationManager.AppSettings["CookieName"];
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, email, DateTime.Now, DateTime.Now.AddMinutes(30), false, new JavaScriptSerializer().Serialize(user), FormsAuthentication.FormsCookiePath);
            string encTicket = FormsAuthentication.Encrypt(ticket);
            HttpContext.Current.Response.Cookies.Add(new HttpCookie(cookieName, encTicket));
        }
        catch (SqlException e)
        {
            throw e;
        }
        finally
        {
            sqlConnection.Close();
        }
    }

}