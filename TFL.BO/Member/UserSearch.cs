﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserSearch
/// </summary>
namespace TFL.BO
{
    public class UserSearch
    {
        public int Userid { get; set; }
        public string FullName { get; set; }
        public string Location { get; set; }
        public int ProfessionType { get; set; }
        public string UserPath { get; set; }
        public string ImagePath { get; set; }
    }
}