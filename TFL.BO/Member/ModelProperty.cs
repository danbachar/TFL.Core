﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ModelProperty
/// </summary>
public class ModelProperty
{
    //the original values or from(in search page)
    public string Hair { get; set; }
    public string Eyes { get; set; }
    public decimal Bust { get; set; }
    public decimal Height { get; set; }
    public decimal Waist { get; set; }
    public decimal Hipe { get; set; }
    public decimal Shoe { get; set; }


    public decimal BustTo { get; set; }
    public decimal HeightTo { get; set; }
    public decimal WaistTo { get; set; }
    public decimal HipeTo { get; set; }
    public decimal ShoeTo { get; set; }

}