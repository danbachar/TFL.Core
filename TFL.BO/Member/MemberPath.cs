﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MemberPath
/// </summary>
public class MemberPath
{
    public static string GetMemberPath(int userid, string firstName, string lastName, int typeid)
    {
        return GetMemberPathByTypeID(typeid) + UpperFirstLetter(firstName) + (lastName != "" ? "." + UpperFirstLetter(lastName) : "") + "." + userid;
    }

    public static string GetMemberPath(UserData user)
    {
        return GetMemberPath(user.UserID, user.FirstName, user.LastName, (int)user.UserType);
    }

    public static string UpperName(string str)
    {
        return string.Join(" ", str.Split(' ').Select(UpperFirstLetter));
    }

    private static string UpperFirstLetter(string input)
    {
        if (input != "")
            return (input.First().ToString().ToUpper() + input.Substring(1)).Replace(" ", "");
        return "";
    }

    public static string GetMemberPathByTypeID(int typeid)
    {
        switch (typeid)
        {
            case 0:
                return "AdminMember/";
            case 1:
                return "Author/";
            case 2:
                return "Fan/";
            case 3:
                return "Client/";
            case 4:
                return "Photographer/";
            case 5:
                return "Model/";
            case 6:
                return "Stylist/";
            case 7:
                return "Hair/";
            case 8:
                return "Makeup/";
            case 9:
                return "Manicure/";
            case 10:
                return "Actor/";
            case 11:
                return "Singer/";
            case 12:
                return "Dancer/";
            case 14:
                return "Agencies/";
        }
        return "Member/";
    }
}