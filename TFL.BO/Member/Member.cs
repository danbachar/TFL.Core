﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Resources;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for Member
/// </summary>
public class Member
{
    public int UserId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string FullName { get; set; }
    public int UserType { get; set; }
    public string ProfessionName { get; set; }
    public bool Gender { get; set; }
    public string GenderName { get; set; }
    public DateTime? BirthDate { get; set; }
    public string BirthDateString { get; set; }
    public string BirthLocation { get; set; }
    public string Biography { get; set; }
    public string Website { get; set; }
    public string Email { get; set; }
    public string Profile { get; set; }
    public ModelProperty ModelProperty { get; set; }
    public bool HasCampaigns { get; set; }
    public List<Campaign> UserCampaigns { get; set; }
    public bool HasNews { get; set; }
    public bool HasModels { get; set; }
    public int NudeMode { get; set; }


    public int GenderSelect { get; set; }
    public string UserPath { get; set; }
    public List<Agency> Agencies { get; set; }
    public string CityID { get; set; }
    public string CountryID { get; set; }
    public string LastLoginDate { get; set; }
    public bool IsLocked { get; set; }
    public string LastLockedDate { get; set; }
    public bool AgreeForMails { get; set; }
    public string CreateDate { get; set; }

    public static Member GetMember(int typeid, string userpath)
    {
        return SqlMember.GetMember(typeid, userpath);
    }

    public static List<Member> SearchMembers(Member member)
    {
        return SqlMember.SearchMembers(member);
    }

    public static void UpdateProfession(int userid, int roleid)
    {
        SqlMember.UpdateProfession(userid, roleid);
    }

    public static void UpdateWebSite(int userid, string website)
    {
        SqlMember.UpdateWebSite(userid, website);
    }
    public static void UpdateBiography(int userid, string bio)
    {
        SqlMember.UpdateBiography(userid, bio);
    }
    public static void UpdateBirthDateAndLocation(int userid, DateTime? birthdate, string countryid, string cityid)
    {
        SqlMember.UpdateBirthDateAndLocation(userid, birthdate, countryid, cityid);
    }

    public static void UpdateModelProperties(int userid, string hair, string eyesid, decimal bust, decimal height, decimal waist, decimal hips, int shoe)
    {
        SqlMember.UpdateModelProperties(userid, hair, eyesid, bust, height, waist, hips, shoe);
    }

    public static Dictionary<string, string> getColorsDictionary()
    {
        string culture = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
        SqlParameter[] parameters = { new SqlParameter("@culture", SqlDbType.VarChar) { Value = culture } };
        DataTable cDataTable = DataAccess.GetDataTable(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString, parameters, "ResourcesByModelColor");
        Dictionary<string, string> dictionary = new Dictionary<string, string>();
        foreach (DataRow keyVal in cDataTable.Rows)
        {
            dictionary.Add(keyVal["resourceKey"].ToString(), keyVal["resourceValue"].ToString());
        }
        return dictionary;
    }

    public static List<int> GetRange(int start, int end, int step)
    {
        List<int> range = new List<int>();
        for (int i = start; i <= end; i += step)
        {
            range.Add(i);
        }
        return range;
    }


    public static void LockOrUnlockUser(int userid)
    {
        SqlMember.LockOrUnlockUser(userid);
    }

    public static void DeleteMember(int userid)
    {
        SqlMember.DeleteMember(userid);
    }

    public static void AddRemoveNewsLetter(int userid)
    {
        SqlMember.AddRemoveNewsLetter(userid);
    }

    public static List<Agency> GetModelAgencies(int userid)
    {
        return SqlMember.GetModelAgencies(userid);
    }

    public static List<Member> GetMembersByTypeID(int typeid, int lastseen)
    {
       return SqlMember.GetMembersByTypeID(typeid, lastseen);
    }
}