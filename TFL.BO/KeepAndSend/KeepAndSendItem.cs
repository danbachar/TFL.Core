﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for KeepAndSendItem
/// </summary>
public class KeepAndSendItem
{
    public int ItemId { get; set; }
    public string ItemTitle { get; set; }
    public string ItemDescription { get; set; }
    public string ItemType { get; set; }
    public string ItemCover { get; set; }
    public string Path { get; set; }
}