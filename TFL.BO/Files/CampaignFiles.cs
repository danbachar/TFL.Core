﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CampaignFiles
/// </summary>
public class CampaignFiles : FileUploaded
{
    public string Description { get; set; }
    public string Tags { get; set; }
    public bool SetAsCover { get; set; }
    public bool SetAsFirst { get; set; }
    public bool IsNude { get; set; }
    public int Place { get; set; }


    public static void SavePhotosIndex(int id, List<CampaignFiles> files)
    {
        files = files.Select(x => new CampaignFiles()
        {
            FileName = Path.GetFileName(x.FileName.Replace("_s", string.Empty)),
            Place = x.Place
        }).ToList();
        SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TopFashionList"].ConnectionString);
        SqlCommand sqlCommand = new SqlCommand("SaveFilesPlaces", sqlConnection);

        sqlCommand.CommandType = CommandType.StoredProcedure;

        sqlCommand.Parameters.Add("@campaignID", SqlDbType.Int).Value = id;
        sqlCommand.Parameters.Add("@filesTable", SqlDbType.Structured).Value = FullCampaign.GetDataTableFiles(files);

        try
        {
            sqlConnection.Open();
            sqlCommand.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            throw e;
        }
        finally
        {
            sqlConnection.Close();
        }
    }
}