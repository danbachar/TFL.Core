﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for FileUploaded
/// </summary>
public class FileUploaded
{
    public string FileName { get; set; }
    public string FilePath { get; set; }
}