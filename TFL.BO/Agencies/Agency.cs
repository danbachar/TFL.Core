﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Agency
/// </summary>
public class Agency
{
    public int AgencyId { get; set; }
    public string AgencyLocation { get; set; }
    public string AgencyName { get; set; }
    public string AgencyLocationID { get; set; }
}